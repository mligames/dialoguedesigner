﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LinkData
{
    [SerializeField] private string nodeLinkId;
    public string NodeLinkId { get => nodeLinkId; set => nodeLinkId = value; }

    [SerializeField] private string parentNodeId;
    public string ParentNodeId { get => parentNodeId; set => parentNodeId = value; }

    [SerializeField] private string childNodeId;
    public string ChildNodeId { get => childNodeId; set => childNodeId = value; }

    [SerializeField] private int orderId;
    public int OrderId { get => orderId; set => orderId = value; }

    [SerializeField] private string linkText;
    public string LinkText { get => linkText; set => linkText = value; }

    [SerializeField] private string choiceId;
    public string ChoiceId { get => choiceId; set => choiceId = value; }

    [SerializeField] private string weight;
    public string Weight { get => weight; set => weight = value; }

    [SerializeField] private string createdBy;
    public string CreatedBy { get => createdBy; set => createdBy = value; }

    [SerializeField] private string createdDate;
    public string CreatedDate { get => createdDate; set => createdDate = value; }

    [SerializeField] private string modifiedBy;
    public string ModifiedBy { get => modifiedBy; set => modifiedBy = value; }

    [SerializeField] private string modifiedDate;
    public string ModifiedDate { get => modifiedDate; set => modifiedDate = value; }

    public LinkController AssignedLinkController { get; set; }
    public NodeController ParentController { get; set; }
    public NodeController ChildController { get; set; }
    public Tuple<NodeController,NodeController> ParentChildRelation { get; set; }
    public string LinkTargetInfo { get; set; }

    public LinkData()
    {
        NodeLinkId = "NONE";
        ParentNodeId = "NONE";
        ChildNodeId = "NONE";
        OrderId = 0;
        LinkText = "New Link Text";
        ChoiceId = "NA";
        Weight = "NA";
        CreatedBy = "User";
        CreatedDate = System.DateTime.UtcNow.ToString();
        ModifiedBy = "User";
        ModifiedDate = System.DateTime.UtcNow.ToString();
        AssignedLinkController = null;
        ParentController = null;
        ChildController = null;
        ParentChildRelation = null;
        LinkTargetInfo = "No Parent -> No Child";
    }
    public LinkData(string linkId)
    {
        NodeLinkId = linkId;
        ParentNodeId = "NONE";
        ChildNodeId = "NONE";
        OrderId = 0;
        LinkText = "New Link Text";
        ChoiceId = "NA";
        Weight = "NA";
        CreatedBy = "User";
        CreatedDate = System.DateTime.UtcNow.ToString();
        ModifiedBy = "User";
        ModifiedDate = System.DateTime.UtcNow.ToString();
        AssignedLinkController = null;
        ParentController = null;
        ChildController = null;
        ParentChildRelation = null;
        LinkTargetInfo = "No Parent -> No Child";
    }
    //public LinkData(NodeController parentNode, NodeController childNode)
    //{
    //    NodeLinkId = IDGenerator.MD5ID(GetHashCode());
    //    ParentNodeId = parentNode.NodeId;
    //    ChildNodeId = childNode.NodeId;
    //    OrderId = 0;
    //    LinkText = "New Link Text";
    //    ChoiceId = "NA";
    //    Weight = "NA";
    //    CreatedBy = "User";
    //    CreatedDate = System.DateTime.UtcNow.ToString();
    //    ModifiedBy = "User";
    //    ModifiedDate = System.DateTime.UtcNow.ToString();
    //    AssignedLinkController = null;
    //    ParentController = parentNode;
    //    ChildController = childNode;
    //    ParentChildRelation = new Tuple<NodeController, NodeController>(parentNode, childNode);
    //    LinkTargetInfo = parentNode.NodeTitle + " -> " + childNode.NodeTitle;
    //}
    //public LinkData(LinkController linkController)
    //{
    //    NodeLinkId = IDGenerator.MD5ID(GetHashCode());
    //    ParentNodeId = parentNode.NodeId;
    //    ChildNodeId = childNode.NodeId;
    //    OrderId = 0;
    //    LinkText = "New Link Text";
    //    ChoiceId = "NA";
    //    Weight = "NA";
    //    CreatedBy = "User";
    //    CreatedDate = System.DateTime.UtcNow.ToString();
    //    ModifiedBy = "User";
    //    ModifiedDate = System.DateTime.UtcNow.ToString();
    //    AssignedLinkController = null;
    //    ParentController = parentNode;
    //    ChildController = childNode;
    //    ParentChildRelation = new Tuple<NodeController, NodeController>(parentNode, childNode);
    //    LinkTargetInfo = parentNode.NodeTitle + " -> " + childNode.NodeTitle;
    //}
    public LinkData(LinkData importData)
    {
        NodeLinkId = importData.nodeLinkId;
        ParentNodeId = importData.parentNodeId;
        ChildNodeId = importData.childNodeId;
        OrderId = importData.orderId;
        LinkText = importData.linkText;
        ChoiceId = importData.choiceId;
        Weight = importData.weight;
        CreatedBy = importData.createdBy;
        CreatedDate = importData.createdDate;
        ModifiedBy = importData.modifiedBy;
        ModifiedDate = importData.modifiedDate;
    }

    public void ImportModelData(LinkData importData)
    {
        NodeLinkId = importData.nodeLinkId;
        ParentNodeId = importData.parentNodeId;
        ChildNodeId = importData.childNodeId;
        OrderId = importData.orderId;
        LinkText = importData.linkText;
        ChoiceId = importData.choiceId;
        Weight = importData.weight;
        CreatedBy = importData.createdBy;
        CreatedDate = importData.createdDate;
        ModifiedBy = importData.modifiedBy;
        ModifiedDate = importData.modifiedDate;
    }

    public LinkDataModel ExportModelData()
    {
        return new LinkDataModel(nodeLinkId, parentNodeId, childNodeId, orderId, linkText, choiceId, weight, createdBy, createdDate, modifiedBy, modifiedDate);
    }
    public override string ToString()
    {
        string parentName = "No Parent";
        if (AssignedLinkController.ParentNode != null)
        {
            parentName = AssignedLinkController.ParentNode.NodeData.NodeTitle;
        }

        string childName = "No Child";
        if (AssignedLinkController.ChildNode != null)
        {
            childName = AssignedLinkController.ChildNode.NodeData.NodeTitle;
        }

        return parentName + " -> " + childName;
    }
}

