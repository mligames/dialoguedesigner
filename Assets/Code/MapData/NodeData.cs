﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class NodeData
{
    [SerializeField] private string nodeId;
    public string NodeId { get => nodeId; set => nodeId = value; }

    [SerializeField] private string caseId;
    public string CaseId { get => caseId; set => caseId = value; }

    [SerializeField] private string nodeTitle;
    public string NodeTitle { get => nodeTitle; set => nodeTitle = value; }

    [SerializeField] private string nodeText;
    public string NodeText { get => nodeText; set => nodeText = value; }

    [SerializeField] private NodeType nodeType;
    public NodeType NodeType { get => nodeType; set => nodeType = value; }

    [SerializeField] private string mediaId;
    public string MediaId { get => mediaId; set => mediaId = value; }

    [SerializeField] private string questionId;
    public string QuestionId { get => questionId; set => questionId = value; }

    [SerializeField] private bool isTerminalNode;
    public bool IsTerminalNode { get => isTerminalNode; set => isTerminalNode = value; }

    [SerializeField] private List<LinkData> nodeLinks;
    public List<LinkData> NodeLinks { get => nodeLinks; set => nodeLinks = value; }

    [SerializeField] private NodeCoordinates nodeCoordinates;
    public NodeCoordinates NodeCoordinates { get => nodeCoordinates; set => nodeCoordinates = value; }

    [SerializeField] protected QuestionData questionData;
    public QuestionData QuestionData { get => questionData; set => questionData = value; }
    public NodeData()
    {
        NodeId = "NONE";
        CaseId = "";
        NodeTitle = "New Node";
        NodeText = "Placeholder node text";
        NodeType = NodeType.NARRATIVE;
        MediaId = "";
        QuestionId = "";
        IsTerminalNode = false;
        NodeLinks = new List<LinkData>();
        NodeCoordinates = new NodeCoordinates();
        QuestionData = new QuestionData();
    }
    public NodeData(string nodeId)
    {
        NodeId = nodeId;
        CaseId = "";
        NodeTitle = "New Node";
        NodeText = "Placeholder node text";
        NodeType = NodeType.NARRATIVE;
        MediaId = "";
        QuestionId = "";
        IsTerminalNode = false;
        NodeLinks = new List<LinkData>();
        NodeCoordinates = new NodeCoordinates();
        QuestionData = new QuestionData();
    }
    public void ImportData(NodeData nodeData)
    {
        NodeId = nodeData.NodeId;
        CaseId = nodeData.CaseId;
        NodeTitle = nodeData.NodeTitle;
        NodeText = nodeData.NodeText;
        NodeType = nodeData.NodeType;
        MediaId = nodeData.MediaId;
        QuestionId = nodeData.QuestionId;
        IsTerminalNode = nodeData.IsTerminalNode;
        NodeLinks = nodeData.NodeLinks;
        NodeCoordinates = nodeData.NodeCoordinates;
        QuestionData = nodeData.QuestionData;
    }

    public void InitBaseData(string nodeId, string caseId, string nodeTitle, string nodeText, NodeType nodeType, string mediaId, string questionId, bool isTerminalNode, List<LinkData> nodeLinks, NodeCoordinates nodeCoordinates, QuestionData questionData)
    {
        NodeId = nodeId;
        CaseId = caseId;
        NodeTitle = nodeTitle;
        NodeText = nodeText;
        NodeType = nodeType;
        MediaId = mediaId;
        QuestionId = questionId;
        IsTerminalNode = isTerminalNode;
        NodeLinks = nodeLinks;
        NodeCoordinates = nodeCoordinates;
        QuestionData = questionData;
    }
}
