﻿using System;
using UnityEngine;

[Serializable]
public class NodeCoordinates
{
    [SerializeField] private string nodeId;
    public string NodeId { get => nodeId; set => nodeId = value; }

    [SerializeField] private float nodeX;
    public float NodeX { get => nodeX; set => nodeX = value; }

    [SerializeField] private float nodeY;
    public float NodeY { get => nodeY; set => nodeY = value; }

    [SerializeField] private string nodeColor;
    public string NodeColor { get => nodeColor; set => nodeColor = value; }

    [SerializeField] private string borderColor;
    public string BorderColor { get => borderColor; set => borderColor = value; }

}
