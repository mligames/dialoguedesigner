﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class QuestionData
{

    [SerializeField] private string nodeId;
    public string NodeId { get => nodeId; set => nodeId = value; }

    [SerializeField] private NodeType nodeType;
    public NodeType NodeType { get => nodeType; set => nodeType = value; }

    [SerializeField] private string questionId;
    public string QuestionId { get => questionId; set => questionId = value; }

    [SerializeField] private string choiceId;
    public string ChoiceId { get => choiceId; set => choiceId = value; }

    [SerializeField] private int orderId;
    public int OrderId { get => orderId; set => orderId = value; }

    [SerializeField] private string questionText;
    public string QuestionText { get => questionText; set => questionText = value; }

    [SerializeField] private string choiceText;
    public string ChoiceText { get => choiceText; set => choiceText = value; }

    [SerializeField] private List<ChoiceData> questionChoices;
    public List<ChoiceData> QuestionChoices { get => questionChoices; set => questionChoices = value; }

    public QuestionData()
    {
        NodeId = "NA";
        NodeType = NodeType.NARRATIVE;
        QuestionId = "NA";
        ChoiceId = "NA";
        OrderId = 0;
        QuestionText = "Placeholder Question Text";
        ChoiceText = "Placeholder Question Choice Text";
        QuestionChoices = new List<ChoiceData>();
    }

    public void InitQuestionData(string nodeId, NodeType nodeType, string questionId, string choiceId, int orderId, string questionText, string choiceText, List<ChoiceData> questionChoices)
    {
        this.NodeId = nodeId;
        this.NodeType = nodeType;
        this.QuestionId = questionId;
        this.ChoiceId = choiceId;
        this.OrderId = orderId;
        this.QuestionText = questionText;
        this.ChoiceText = choiceText;
        QuestionChoices = questionChoices;
    }
}
