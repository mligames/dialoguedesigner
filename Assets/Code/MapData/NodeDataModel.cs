﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public struct NodeDataModel
{
    public string nodeId;
    public string caseId;
    public string nodeTitle;
    public string nodeText;
    public NodeType nodeType;
    public string mediaId;
    public string questionId;
    public bool isTerminalNode;
    public List<LinkDataModel> nodeLinks;
    public NodeCoordinates nodeCoordinates;

    public NodeDataModel(string nodeId, string caseId, string nodeTitle, string nodeText, NodeType nodeType, string mediaId, string questionId, bool isTerminalNode, List<LinkDataModel> nodeLinks, NodeCoordinates nodeCoordinates)
    {
        this.nodeId = nodeId;
        this.caseId = caseId;
        this.nodeTitle = nodeTitle;
        this.nodeText = nodeText;
        this.nodeType = nodeType;
        this.mediaId = mediaId;
        this.questionId = questionId;
        this.isTerminalNode = isTerminalNode;
        this.nodeLinks = new List<LinkDataModel>();
        for (int i = 0; i < nodeLinks.Count; i++)
        {
            this.nodeLinks.Add(nodeLinks[i]);
        }
        this.nodeCoordinates = nodeCoordinates;
    }
}
