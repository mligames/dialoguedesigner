﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ChoiceData
{
    public string choiceId;
    public string questionId;
    public int orderId;
    public string choiceText;
    public string feedback;
    public bool correct;

    public ChoiceData()
    {
        choiceId = "NA";
        questionId = "NA";
        orderId = 0;
        choiceText = "NA";
        feedback = "NA";
        correct = false;
    }

    public ChoiceData(QuestionData questionData)
    {
        choiceId = IDGenerator.GenerateID();
        questionId = questionData.QuestionId;
        orderId = 0;
        choiceText = "NA";
        feedback = "NA";
        correct = false;
    }

    public void InitChoiceData(string choiceId, string questionId, int orderId, string choiceText, string feedback, bool correct)
    {
        this.choiceId = choiceId;
        this.questionId = questionId;
        this.orderId = orderId;
        this.choiceText = choiceText;
        this.feedback = feedback;
        this.correct = correct;
    }

}
