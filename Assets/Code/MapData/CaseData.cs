﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newTestData", menuName = "VPSim/Test Data")]
public class CaseData : ScriptableObject
{
    public string caseId;
    public string caseName;
    public string caseDesc;
    public string caseAuthor;
    public string caseAuthorId;
    public string caseCreatedDate;
    public string caseLastModified;
    public int caseInstitutionId;
    public string learningSummary;
    public string startNode;
    public bool showRestartButton;
    public int navigationMode;
    public bool showEndReport;
    public string caseAbstract;
    public string instructionsToEducator;
    public string keywords;
    public bool showLearningSummary;
    public string otherTargetAudience;
    public bool isTemplate;
    public int accessMode;
    public bool restrictNodeCopy;
    public string caseLastModifiedBy;
    public bool caseDeleted;
    public bool casePublished;
    public string dateCasePublished;
    public string casePublishedBy;
    public bool showReadOnlyMap;
    public bool hideFromCaseList;
    public bool caseResumable;
    public List<NodeData> nodeList;
    public List<CounterData> counterList;
}
