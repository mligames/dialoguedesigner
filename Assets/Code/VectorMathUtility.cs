﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorMathUtility
{
    public static Vector2 MidPointBetweenVector2(Vector2 posA, Vector2 posB)
    {
        return (posA + posB) / 2;
    }
	public static Vector3 MidPointBetweenVector3(Vector3 posA, Vector3 posB)
	{
		return (posA + posB) / 2;
	}
	public static Vector2 CrossProduct(Vector2 posA, Vector2 posB)
    {
        Vector2 direction = posB - posA;
        Vector2 crossVector = new Vector2(direction.y, -direction.x);
        return crossVector;
    }
	/// <summary>
	/// A helper function that finds the average position of several component objects, 
	/// specifically because they have transforms
	/// </summary>
	/// <param name="components">
	/// The list of components to average
	/// </param>
	/// <typeparam name="TComponent">
	/// The Unity Component which has a transform
	/// </typeparam>
	/// <returns>
	/// The average position
	/// </returns>
	public static Vector3 FindAveragePosition<TComponent>(TComponent[] components) where TComponent : Component
	{
		Vector3 output = Vector3.zero;
		foreach (TComponent component in components)
		{
			if (component == null)
			{
				continue;
			}
			output += component.transform.position;
		}
		return output / components.Length;
	}
	/// <summary>
	/// A helper function that finds the average position of several component objects, 
	/// specifically because they have transforms
	/// </summary>
	/// <param name="components">
	/// The list of components to average
	/// </param>
	/// <typeparam name="TComponent">
	/// The Unity Component which has a transform
	/// </typeparam>
	/// <returns>
	/// The average position
	/// </returns>
	public static Vector3 FindAveragePosition<TComponent>(List<TComponent> components) where TComponent : Component
	{
		Vector3 output = Vector3.zero;
		foreach (TComponent component in components)
		{
			if (component == null)
			{
				continue;
			}
			output += component.transform.position;
		}
		return output / components.Count;
	}
}
