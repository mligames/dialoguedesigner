﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class DebugOutput : Singleton<DebugOutput>
{
    [SerializeField] private TMP_InputField textField;
    public TMP_InputField TextField { get => textField; set => textField = value; }

    // Start is called before the first frame update
    void Start()
    {
        textField.text = "Log Begin";
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Log(string value)
    {
        string currentText = textField.text;
        string appendText = "\n" + value;
        textField.text = currentText + appendText;
    }
}
