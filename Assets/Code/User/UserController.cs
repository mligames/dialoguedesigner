﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Subclass EventGameState using scoped enum.  
/// </summary>

public enum UserState { IDLE, PANNING, CREATING_NODE, DRAGGING_NODE }

public class UserController : Singleton<UserController>
{
    [Header("State")]
    [SerializeField] private UserState currentState;
    public UserState CurrentState { get => currentState; set => currentState = value; }

    //[SerializeField] private string currentStateName;
    //public string CurrentStateName { get => currentStateName; set => currentStateName = value; }

    //[SerializeField] private UserInputState currentState;
    //public UserInputState CurrentState { get => currentState; set => currentState = value; }

    [Header("Variables")]
    Vector2 mouseClickPos;
    public Vector2 MouseWorldPoint
    {
        get
        {
            return Camera.main.ScreenToWorldPoint(
                new Vector3(Input.mousePosition.x,
                Input.mousePosition.y,
                Camera.main.nearClipPlane));
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case UserState.IDLE:
                break;
            case UserState.PANNING:
                break;
            case UserState.CREATING_NODE:
                break;
            case UserState.DRAGGING_NODE:
                break;
        }
    }
    private void LateUpdate()
    {
        if (CurrentState == UserState.PANNING)
        {
            //var distance = MouseWorldPoint - mouseClickPos;
            Vector2 cameraPos = Camera.main.transform.position;
            Vector2 movement = cameraPos + new Vector2(-MouseWorldPoint.x, -MouseWorldPoint.y);
            transform.position = movement;
            //var distance = MouseWorldPoint - mouseClickPos;
            //Vector3 movement = Camera.main.transform.position + new Vector3(-distance.x, -distance.y, 0);
            //movement.z = -1;
            //transform.position = movement;
        }
    }
    public void ChangeState(UserState state)
    {
        currentState = state;
    }
    
}