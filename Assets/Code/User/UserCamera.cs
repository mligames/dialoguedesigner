﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserCamera : Singleton<UserCamera>
{
    private NodeViewPanel nodeViewPanel;

    [Header("Variables")]
    Vector2 mouseClickPos;
    public Vector2 MouseWorldPoint
    {
        get
        {
            return Camera.main.ScreenToWorldPoint(
                new Vector3(Input.mousePosition.x,
                Input.mousePosition.y,
                Camera.main.nearClipPlane));
        }
    }
    [SerializeField] private bool isPanningBlocked;
    public bool IsPanningBlocked { get => isPanningBlocked; set => isPanningBlocked = value; }

    [SerializeField] private bool isPanning;
    public bool IsPanning { get => isPanning; set => isPanning = value; }


    private void Start()
    {
        NodeViewManager.Instance.onOverInpector.AddListener(SetPanningBlocked);
    }
    // Update is called once per frame
    void Update()
    {
        if (isPanningBlocked)
        {
            IsPanning = false;
            return;
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) && !IsPanning)
            {
                mouseClickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                IsPanning = true;
            }

            // If LMB is released, stop moving the camera
            if (Input.GetKeyUp(KeyCode.Mouse0))
                IsPanning = false;
        }

    }
    // Update is called once per frame
    private void LateUpdate()
    {
        if (IsPanning)
        {
            var distance = MouseWorldPoint - mouseClickPos;
            Vector2 cameraPos = Camera.main.transform.position;
            Vector2 movement = cameraPos + new Vector2(-distance.x, -distance.y);
            transform.position = movement;
        }
    }
    private void SetPanningBlocked(bool value)
    {
        isPanningBlocked = value;
    }
}
