﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Collider2D))]
public class MapObjectClickHandler : MonoBehaviour
{
    public Collider2D ColliderComp;

    // Start is called before the first frame update

    [Header("Variables")]
    Vector2 mouseClickPos;
    public Vector2 MouseWorldPoint
    {
        get
        {
            return Camera.main.ScreenToWorldPoint(
                new Vector3(Input.mousePosition.x,
                Input.mousePosition.y,
                Camera.main.nearClipPlane));
        }
    }

    public float clicked = 0;
    public float clicktime = 0;
    public float clickdelay = 0.5f;

    public OnDoubleClick onDoubleClick;

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonUp(0))
        {
            HandleDoubleClick();
        }

    }
    private void HandleDoubleClick()
    {
        clicked++;
        if (clicked == 1)
            clicktime = Time.time;

        if (clicked > 1 && Time.time - clicktime < clickdelay)
        {
            clicked = 0;
            clicktime = 0;
            onDoubleClick.Invoke();
            //Debug.Log("Double Clicked!...");
        }
        else if (clicked > 2 || Time.time - clicktime > 1)
        {
            clicked = 0;
        }

    }

}
