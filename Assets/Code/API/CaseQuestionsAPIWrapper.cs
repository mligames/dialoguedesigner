﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CaseQuestionsAPIWrapper : ScriptableObject
{
    public QuestionData[] AllQuestionData;
}
