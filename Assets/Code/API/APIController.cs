﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

[Serializable] public class CaseDataLoaded : UnityEvent<bool, CaseData> { }
[Serializable] public class CaseQuestionDataLoaded : UnityEvent<bool, CaseQuestionsAPIWrapper> { }

public class APIController : MonoBehaviour
{
    private string defaultContentType = "application/json";
    public string DevelopmentTest = "6bedb187-5833-492a-a5e0-a697b9af0cdf";
    private const string apiUrl = "http://136.142.148.85:8090/api/";
    private string nodeListUrl = apiUrl + "NodeLists/";
    private string caseDataUrl = apiUrl + "VpSimCase/GetCaseNodeObject/";
    private string caseQuestionsUrl = apiUrl + "questionlist/GetQuestionAndChoiceDataByCase/";

    public bool CaseAPISuccess;
    public bool CaseQuestionsAPISuccess;

    [SerializeField] TextAsset testCaseData;
    public TextAsset TestCaseData { get => testCaseData; private set => testCaseData = value; }

    [SerializeField] TextAsset testCaseQuestionsData;
    public TextAsset TestCaseQuestionsData { get => testCaseQuestionsData; private set => testCaseQuestionsData = value; }

    public CaseDataLoaded onCaseDataLoaded;
    public CaseQuestionDataLoaded onCaseQuestionsLoaded;

    #region CaseData Calls
    public string CaseDataUrl(string caseId)
    {
        return caseDataUrl + caseId;
    }
    public void GetRequestCaseData(string url)
    {
        Debug.Log("URL = " + url);

        StartCoroutine(HttpGet(url, (r) => OnCaseDataRequestComplete(r)));
    }

    public void OnCaseDataRequestComplete(Response response)
    {
        if (response.StatusCode.ToString().Equals("200"))
        {
            DebugOutput.Instance.Log("Case Loaded Successfully");
            CaseData caseData = ScriptableObject.CreateInstance<CaseData>();
            JsonUtility.FromJsonOverwrite(response.Data, caseData);
            onCaseDataLoaded.Invoke(true, caseData);
        }
        else
        {
            // Create new local Node Case from Factory
            onCaseDataLoaded.Invoke(false, null);
        }
    }
    #endregion
    
    #region CaseQuestions Calls
    public string CaseQuestionsDataUrl(string caseId)
    {
        return caseQuestionsUrl + caseId;
    }
    public void GetRequestCaseQuestionsData(string url)
    {
        Debug.Log("URL = " + url);

        StartCoroutine(HttpGet(url, (r) => OnCaseQuestionsRequestComplete(r)));
    }

    public void OnCaseQuestionsRequestComplete(Response response)
    {
        if (response.StatusCode.ToString().Equals("200"))
        {
            DebugOutput.Instance.Log("Case Questions Loaded Successfully");
            CaseQuestionsAPIWrapper CaseQuestions = ScriptableObject.CreateInstance<CaseQuestionsAPIWrapper>();

            JsonUtility.FromJsonOverwrite(WrapJsonArray(testCaseQuestionsData.text, "AllQuestionData"), CaseQuestions);
            onCaseQuestionsLoaded.Invoke(true, CaseQuestions);
        }
        else
        {
            onCaseQuestionsLoaded.Invoke(false, null);
        }
    }
    #endregion
    public string NodeDataUrl(string nodeId)
    {
        return nodeListUrl + nodeId;
    }
    public void PutRequestNodeData(NodeData nodeData)
    {
        string url = nodeListUrl + nodeData.NodeId;
        string body = JsonUtility.ToJson(nodeData);
        Debug.Log("PUT "+ body);

        StartCoroutine(HttpPut(url, body, (r) => OnPutRequestNodeData(r)));
    }
    public void OnPutRequestNodeData(Response response)
    {
        Debug.Log("PutRequestResponse: " + response.StatusCode);
        if (response.StatusCode.ToString().Equals("200"))
        {
            Debug.Log("PutRequest Successful " + response.Data);
        }
        else
        {
            Debug.Log("PutRequest Unsuccessful " + response.Data);
        }
    }

    #region API GET POST PUT DELETE
    public IEnumerator HttpGet(string url, System.Action<Response> callback)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                callback(new Response
                {
                    StatusCode = webRequest.responseCode,
                    Error = webRequest.error,
                });
            }

            if (webRequest.isDone)
            {
                string data = System.Text.Encoding.UTF8.GetString(webRequest.downloadHandler.data);
                callback(new Response
                {
                    StatusCode = webRequest.responseCode,
                    Error = webRequest.error,
                    Data = data
                });
            }
        }
    }
    public IEnumerator HttpDelete(string url, System.Action<Response> callback)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Delete(url))
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                callback(new Response
                {
                    StatusCode = webRequest.responseCode,
                    Error = webRequest.error
                });
            }

            if (webRequest.isDone)
            {
                callback(new Response
                {
                    StatusCode = webRequest.responseCode
                });
            }
        }
    }

    public IEnumerator HttpPost(string url, string body, System.Action<Response> callback, IEnumerable<RequestHeader> headers = null)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Post(url, body))
        {
            if (headers != null)
            {
                foreach (RequestHeader header in headers)
                {
                    webRequest.SetRequestHeader(header.Key, header.Value);
                }
            }

            webRequest.uploadHandler.contentType = defaultContentType;
            webRequest.uploadHandler = new UploadHandlerRaw(System.Text.Encoding.UTF8.GetBytes(body));

            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                callback(new Response
                {
                    StatusCode = webRequest.responseCode,
                    Error = webRequest.error
                });
            }

            if (webRequest.isDone)
            {
                string data = System.Text.Encoding.UTF8.GetString(webRequest.downloadHandler.data);
                callback(new Response
                {
                    StatusCode = webRequest.responseCode,
                    Error = webRequest.error,
                    Data = data
                });
            }
        }
    }

    public IEnumerator HttpPut(string url, string body, System.Action<Response> callback, IEnumerable<RequestHeader> headers = null)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Put(url, body))
        {
            if (headers != null)
            {
                foreach (RequestHeader header in headers)
                {
                    webRequest.SetRequestHeader(header.Key, header.Value);
                }
            }

            webRequest.uploadHandler.contentType = defaultContentType;
            webRequest.uploadHandler = new UploadHandlerRaw(System.Text.Encoding.UTF8.GetBytes(body));

            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                callback(new Response
                {
                    StatusCode = webRequest.responseCode,
                    Error = webRequest.error,
                });
            }

            if (webRequest.isDone)
            {
                callback(new Response
                {
                    StatusCode = webRequest.responseCode,
                });
            }
        }
    }

    public IEnumerator HttpHead(string url, System.Action<Response> callback)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Head(url))
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                callback(new Response
                {
                    StatusCode = webRequest.responseCode,
                    Error = webRequest.error,
                });
            }

            if (webRequest.isDone)
            {
                var responseHeaders = webRequest.GetResponseHeaders();
                callback(new Response
                {
                    StatusCode = webRequest.responseCode,
                    Error = webRequest.error,
                    Headers = responseHeaders
                });
            }
        }
    }
    #endregion

    #region TEST LOCAL JSON

    public void GetLocalJsonCaseData()
    {
        Debug.Log("Getting Local Json Test Data");
        CaseData caseData = ScriptableObject.CreateInstance<CaseData>();
        JsonUtility.FromJsonOverwrite(testCaseData.text, caseData);
        onCaseDataLoaded.Invoke(true, caseData);
    }

    public void GetLocalJsonCaseQuestionDataData()
    {
        Debug.Log("CaseData created callback complete...");

        CaseQuestionsAPIWrapper CaseQuestions = ScriptableObject.CreateInstance<CaseQuestionsAPIWrapper>();

        JsonUtility.FromJsonOverwrite(WrapJsonArray(testCaseQuestionsData.text, "AllQuestionData"), CaseQuestions);
        onCaseQuestionsLoaded.Invoke(true, CaseQuestions);
    }
    #endregion

    #region Helpers
    public string WrapJsonArray(string source, string topClass)
    {
        return string.Format("{{ \"{0}\": {1}}}", topClass, source);
    }
    #endregion
}
