﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using TMPro;
using UnityEngine;

public static class NodeDataFactory
{

    //#region Link Data
    //public static LinkData CreateLinkData(LinkDataModel linkDataModel)
    //{
    //    LinkData linkData = new LinkData();
    //    linkData.NodeLinkId = linkDataModel.nodeLinkId;

    //    linkData.ParentNodeId = linkDataModel.parentNodeId;
    //    linkData.ChildNodeId = linkDataModel.childNodeId;
    //    // OrderId
    //    linkData.LinkText = linkDataModel.linkText;
    //    // Choice Id
    //    // Weight
    //    // Created By
    //    linkData.CreatedDate = linkDataModel.createdDate;
    //    // Modified By
    //    linkData.ModifiedDate = linkDataModel.modifiedBy;

    //    return linkData;
    //}
    //public static LinkData CreateLinkData(NodeController parentController, NodeController childController)
    //{
    //    LinkData linkData = ScriptableObject.CreateInstance<LinkData>();
    //    linkData.NodeLinkId = IDGenerator.MD5ID(linkData.GetInstanceID());

    //    linkData.ParentNodeId = parentController.NodeData.NodeId;
    //    linkData.ChildNodeId = childController.NodeData.NodeId;
    //    // OrderId
    //    linkData.LinkText = "New Link";
    //    // Choice Id
    //    // Weight
    //    // Created By
    //    linkData.CreatedDate = System.DateTime.UtcNow.ToString();
    //    // Modified By
    //    linkData.ModifiedDate = System.DateTime.UtcNow.ToString();
    //    linkData.LinkTargetInfo = parentController.NodeData.NodeTitle + " -> " + childController.NodeData.NodeTitle;
    //    linkData.ParentController = parentController;
    //    linkData.ChildController = childController;
    //    linkData.ParentChildRelation = new Tuple<NodeController, NodeController>(parentController, childController);
    //    return linkData;
    //}
    //public static LinkData CopyLinkData(LinkDataModel linkDataModel)
    //{
    //    LinkData linkData = ScriptableObject.CreateInstance<LinkData>();
    //    linkData.NodeLinkId = linkDataModel.nodeLinkId;

    //    linkData.ParentNodeId = linkDataModel.parentNodeId;
    //    linkData.ChildNodeId = linkDataModel.childNodeId;
    //    // OrderId
    //    linkData.LinkText = linkDataModel.linkText;
    //    // Choice Id
    //    // Weight
    //    // Created By
    //    linkData.CreatedDate = linkDataModel.createdDate;
    //    // Modified By
    //    linkData.ModifiedDate = System.DateTime.UtcNow.ToString();

    //    //if(DesignerManager.Instance.NodeDictionary.TryGetValue(linkDataModel.parentNodeId, out NodeController parentNode))
    //    //{

    //    //}
    //    //linkData.LinkTargetInfo = parentController.NodeData.NodeTitle + " -> " + childController.NodeData.NodeTitle;
    //    //linkData.ParentController = parentController;
    //    //linkData.ChildController = childController;
    //    //linkData.ParentChildRelation = new Tuple<NodeController, NodeController>(parentController, childController);
    //    return linkData;
    //}
    ////public static NodeLinkController CreateNodeLinkConnector()
    ////{
    ////    var prefab = Resources.Load<NodeLinkController>(LinkConnectionPrefabPath);
    ////    NodeLinkController instance = GameObject.Instantiate(prefab, null);
    ////    return instance;
    ////}

    ////public static NodeLinkController CreateNodeLinkConnection(NodeController parentController, NodeController childController)
    ////{
    ////    NodeLinkController nodeLink = CreateNodeLinkConnector();
    ////    nodeLink.SetNodeLinkData(CreateNodeLinkData(parentController, childController));
    ////    nodeLink.AssignParentNodeController(parentController);
    ////    nodeLink.AssignChildNodeController(childController);
    ////    parentController.AddRelatedNodeLink(nodeLink);
    ////    //nodeLink.gameObject.name = "NodeLink(" + parentController.NodeTitle + " -> " + childController.NodeTitle + ")";
    ////    nodeLink.gameObject.name = "NodeLink(" + nodeLink.NodeLinkData.NodeLinkId + ")";
    ////    return nodeLink;
    ////}
    //#endregion


    //#region NodeBaseData

    ////public static NodeDataModel ChangeBaseDataType(NodeDataModel nodeProxyData, NodeType nodeType)
    ////{
    ////    return new NodeDataModel(
    ////        nodeProxyData.nodeId,
    ////        nodeProxyData.caseId,
    ////        nodeProxyData.nodeTitle,
    ////        nodeProxyData.nodeText,
    ////        nodeType,
    ////        nodeProxyData.mediaId,
    ////        nodeProxyData.questionId,
    ////        nodeProxyData.isTerminalNode,
    ////        nodeProxyData.nodeLinks,
    ////        nodeProxyData.nodeCoordinates
    ////        );
    ////}
    //#endregion

    //#region Narrative
    //public static NarrativeNodeData CreateNewNarrativeNodeData()
    //{
    //    NarrativeNodeData nodeData = new NarrativeNodeData();
    //    nodeData.NodeId = IDGenerator.MD5ID(nodeData.GetHashCode());
    //    nodeData.CaseId = DesignerManager.Instance.CaseData.caseId;
    //    nodeData.NodeTitle = "New Node";
    //    nodeData.NodeText = "Placeholder node text";
    //    nodeData.NodeType = NodeType.NARRATIVE;
    //    nodeData.MediaId = "";
    //    nodeData.QuestionId = "";
    //    nodeData.IsTerminalNode = false;
    //    nodeData.NodeLinks = new List<LinkData>();
    //    nodeData.NodeCoordinates = new NodeCoordinates();
    //    return nodeData;
    //}

    //public static NarrativeNodeData LoadNarrativeFromNodeData(NodeData importData)
    //{
    //    NarrativeNodeData nodeData = new NarrativeNodeData();
    //    nodeData.ImportData(importData);
    //    return nodeData;
    //}
    //public static NarrativeNodeData CreateCopyNarrativeNodeData(NarrativeNodeData originalNode)
    //{
    //    NarrativeNodeData nodeData = new NarrativeNodeData();
    //    nodeData.NodeId = IDGenerator.MD5ID(nodeData.GetHashCode());
    //    nodeData.CaseId = DesignerManager.Instance.CaseData.caseId;
    //    nodeData.NodeTitle = originalNode.NodeTitle;
    //    nodeData.NodeText = originalNode.NodeText;
    //    nodeData.NodeType = originalNode.NodeType;
    //    nodeData.MediaId = originalNode.MediaId;
    //    nodeData.QuestionId = originalNode.QuestionId;
    //    nodeData.IsTerminalNode = originalNode.IsTerminalNode;
    //    nodeData.NodeLinks = new List<LinkData>();

    //    foreach (var item in originalNode.NodeLinks)
    //    {
    //        nodeData.NodeLinks.Add(item);
    //    }

    //    nodeData.NodeCoordinates = new NodeCoordinates();
        
    //    return nodeData;
    //}
    //#endregion

    //#region MCQ
    //public static MCQNodeData CreateMCQNodeData()
    //{
    //    MCQNodeData nodeData = ScriptableObject.CreateInstance<MCQNodeData>();
    //    //nodeData.CopyBaseModelData(CreateNewBaseData(GenerateId(nodeData), NodeType.MCQ));
    //    nodeData.Question = CreateNewQuestionData(nodeData);
    //    return nodeData;
    //}
    //public static MCQNodeData CreateMCQNodeData(NodeDataModel baseData)
    //{
    //    MCQNodeData nodeData = ScriptableObject.CreateInstance<MCQNodeData>();
    //    nodeData.ImportBaseData(baseData);
    //    nodeData.Question = CreateNewQuestionData(nodeData);
    //    return nodeData;
    //}
    //public static MCQNodeData CopyMCQNodeData(MCQNodeData originalNode)
    //{
    //    Debug.Log("TODO: IMPLEMENT DATA COPY");
    //    MCQNodeData nodeData = ScriptableObject.CreateInstance<MCQNodeData>();
    //    //nodeData.InitBaseData(originalNode.NodeId, originalNode.CaseId, originalNode.NodeTitle, originalNode.NodeText, originalNode.NodeType, originalNode.MediaId, originalNode.QuestionId, originalNode.IsTerminalNode, new List<NodeLinkModel>(), originalNode.NodeCoordinates);

    //    nodeData.Question = CopyQuestionData(originalNode.Question);
    //    return nodeData;
    //}
    //#endregion

    //#region Inquiry
    //public static InquiryNodeData CreateInquiryNodeData()
    //{
    //    InquiryNodeData nodeData = ScriptableObject.CreateInstance<InquiryNodeData>();
    //    //nodeData.CopyBaseModelData(CreateNewBaseData(GenerateId(nodeData), NodeType.INQUIRY));
    //    nodeData.QuestionData = CreateNewQuestionData(nodeData);
    //    return nodeData;
    //}
    //public static InquiryNodeData CreateInquiryNodeData(NodeDataModel baseData)
    //{
    //    InquiryNodeData nodeData = ScriptableObject.CreateInstance<InquiryNodeData>();
    //    nodeData.ImportBaseData(baseData);
    //    nodeData.QuestionData = CreateNewQuestionData(nodeData);
    //    return nodeData;
    //}
    //public static InquiryNodeData CopyInquiryNodeData(InquiryNodeData originalNode)
    //{
    //    Debug.Log("TODO: IMPLEMENT DATA COPY");
    //    InquiryNodeData nodeData = ScriptableObject.CreateInstance<InquiryNodeData>();
    //    //nodeData.InitBaseData(originalNode.NodeId, originalNode.CaseId, originalNode.NodeTitle, originalNode.NodeText, originalNode.NodeType, originalNode.MediaId, originalNode.QuestionId, originalNode.IsTerminalNode, new List<NodeLinkModel>(), originalNode.NodeCoordinates);

    //    nodeData.QuestionData = CopyQuestionData(originalNode.QuestionData);

    //    return nodeData;
    //}
    //#endregion

    //#region TextResponse
    //public static TextResponseNodeData CreateTextResponseNodeData()
    //{
    //    TextResponseNodeData nodeData = ScriptableObject.CreateInstance<TextResponseNodeData>();
    //    //nodeData.CopyBaseModelData(CreateNewBaseData(GenerateId(nodeData), NodeType.TEXT_RESPONSE));
    //    nodeData.Question = CreateNewQuestionData(nodeData);
    //    return nodeData;
    //}
    //public static TextResponseNodeData CreateTextResponseNodeData(NodeDataModel baseData)
    //{
    //    TextResponseNodeData nodeData = ScriptableObject.CreateInstance<TextResponseNodeData>();
    //    nodeData.ImportBaseData(baseData);
    //    nodeData.Question = CreateNewQuestionData(nodeData);
    //    return nodeData;
    //}
    //public static TextResponseNodeData CopyTextResponseNodeData(TextResponseNodeData originalNode)
    //{
    //    TextResponseNodeData nodeData = ScriptableObject.CreateInstance<TextResponseNodeData>();
    //    //nodeData.InitBaseData(originalNode.NodeId, originalNode.CaseId, originalNode.NodeTitle, originalNode.NodeText, originalNode.NodeType, originalNode.MediaId, originalNode.QuestionId, originalNode.IsTerminalNode, new List<NodeLinkModel>(), originalNode.NodeCoordinates);
    //    nodeData.Question = CopyQuestionData(originalNode.Question);
    //    return nodeData;
    //}
    //#endregion

    //#region Branch
    //public static BranchingNodeData CreateBranchingNodeData()
    //{
    //    BranchingNodeData nodeData = new BranchingNodeData();
    //    //nodeData.CopyBaseModelData(CreateNewBaseData(GenerateId(nodeData), NodeType.BRANCHING));
    //    return nodeData;
    //}
    //public static BranchingNodeData CreateBranchingNodeData(NodeDataModel baseData)
    //{
    //    BranchingNodeData nodeData = new BranchingNodeData();

    //    //nodeData.ImportBaseData(baseData);
    //    return nodeData;
    //}
    //public static BranchingNodeData CopyBranchingNodeData(BranchingNodeData originalNode)
    //{
    //    BranchingNodeData nodeData = ScriptableObject.CreateInstance<BranchingNodeData>();

    //    //nodeData.InitBaseData(originalNode.NodeId, originalNode.CaseId, originalNode.NodeTitle, originalNode.NodeText, originalNode.NodeType, originalNode.MediaId, originalNode.QuestionId, originalNode.IsTerminalNode, new List<LinkController>(), originalNode.NodeCoordinates);
    //    return nodeData;
    //}
    //#endregion

    //#region Question
    //public static QuestionData CreateNewQuestionData(NodeData nodeData)
    //{
    //    QuestionData questionData = new QuestionData();
    //    questionData.NodeId = nodeData.NodeId;
    //    questionData.NodeType = nodeData.NodeType;
    //    questionData.QuestionId = IDGenerator.MD5ID(questionData.GetHashCode());
    //    questionData.ChoiceId = IDGenerator.MD5ID(questionData.GetHashCode());
    //    questionData.OrderId = 0;
    //    questionData.QuestionText = "Placeholder Question Text";
    //    questionData.ChoiceText = "Placeholder Question Choice Text";
    //    questionData.QuestionChoices = new List<ChoiceData>();
    //    return questionData;
    //}
    //public static QuestionData CopyQuestionData(QuestionData copyData)
    //{
    //    QuestionData questionData = new QuestionData();
    //    questionData.NodeId = copyData.NodeId;
    //    questionData.NodeType = copyData.NodeType;
    //    questionData.QuestionId = IDGenerator.MD5ID(questionData.GetHashCode());
    //    questionData.ChoiceId = IDGenerator.MD5ID(questionData.GetHashCode());
    //    questionData.OrderId = 0;
    //    questionData.QuestionText = "Placeholder Question Text";
    //    questionData.ChoiceText = "Placeholder Question Choice Text";
    //    questionData.QuestionChoices = new List<ChoiceData>();

    //    foreach (var item in copyData.QuestionChoices)
    //    {
    //        if (!questionData.QuestionChoices.Contains(item))
    //        {
    //            questionData.QuestionChoices.Add(CopyChoiceData(item));
    //        }
    //    }

    //    return questionData;
    //}
    //#endregion

    //#region Answer
    //public static ChoiceData CreateNewChoiceData(QuestionData questionData)
    //{
    //    ChoiceData data = new ChoiceData();
    //    data.choiceId = IDGenerator.MD5ID(data.GetHashCode());
    //    data.questionId = questionData.QuestionId;
    //    data.orderId = 0;
    //    data.choiceText = "Placeholder Choice Text";
    //    data.feedback = "Placeholder Feedback Text";
    //    data.correct = false;
    //    return data;
    //}
    //public static ChoiceData CopyChoiceData(ChoiceData copyData)
    //{
    //    ChoiceData data = new ChoiceData();
    //    data.choiceId = copyData.choiceId;
    //    data.questionId = copyData.questionId;
    //    data.orderId = copyData.orderId;
    //    data.choiceText = copyData.choiceText;
    //    data.feedback = copyData.feedback;
    //    data.correct = copyData.correct;
    //    return data;
    //}
    //#endregion
}
