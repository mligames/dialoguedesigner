﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMapObject
{
    void OnHover();
    void OnSelect();
    void OnMouseOver();
}
