﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LinkMapLabel : MonoBehaviour
{
    #region Components
    [Header("Components")]

    [SerializeField] private GameObject linkLabel;
    public GameObject LinkLabel { get => linkLabel; set => linkLabel = value; }

    [SerializeField] private GameObject minimalIcon;
    public GameObject MinimalIcon { get => minimalIcon; set => minimalIcon = value; }
    #endregion

    #region Variables
    [SerializeField] private bool isLabelHidden;
    public bool IsLabelHidden { get => isLabelHidden; set => isLabelHidden = value; }
    #endregion

    public void HideLabel(bool value)
    {
        if (value)
        {
            minimalIcon.SetActive(true);
            linkLabel.SetActive(false);
        }
        else
        {
            minimalIcon.SetActive(false);
            linkLabel.SetActive(true);
        }
    }

}
