﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyLinkLine : MonoBehaviour
{
    [SerializeField] private LineRenderer lineRenderer;
    public LineRenderer LineRenderer { get => lineRenderer; set => lineRenderer = value; }

    public Vector2 ParentPoint
    {
        get
        {
            if (lineRenderer.positionCount > 0)
            {
                return lineRenderer.GetPosition(0);
            }
            else
            {
                return Vector2.zero;
            }
        }
        set
        {
            if (lineRenderer.positionCount > 0)
            {
                lineRenderer.SetPosition(0, value);
            }
        }

    }
    public Vector2 MousePoint
    {
        get
        {
            if (lineRenderer.positionCount > 1)
            {
                return lineRenderer.GetPosition(1);
            }
            else
            {
                return Vector2.zero;
            }
        }
        set
        {
            if (lineRenderer.positionCount > 1)
            {
                lineRenderer.SetPosition(1, value);
            }
        }
    }
    private void Start()
    {
        lineRenderer.positionCount = 2;
    }

}
