﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct LinkDataModel
{
    public string nodeLinkId;
    public string parentNodeId;
    public string childNodeId;
    public int orderId;
    public string linkText;
    public string choiceId;
    public string weight;
    public string createdBy;
    public string createdDate;
    public string modifiedBy;
    public string modifiedDate;

    public LinkDataModel(string nodeLinkId, string parentNodeId, string childNodeId, int orderId, string linkText, string choiceId, string weight, string createdBy, string createdDate, string modifiedBy, string modifiedDate)
    {
        this.nodeLinkId = nodeLinkId;
        this.parentNodeId = parentNodeId;
        this.childNodeId = childNodeId;
        this.orderId = orderId;
        this.linkText = linkText;
        this.choiceId = choiceId;
        this.weight = weight;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.modifiedBy = modifiedBy;
        this.modifiedDate = modifiedDate;
    }
}

