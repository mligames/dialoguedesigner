﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkRelation
{
    public int RelationId { get; set; }
    public Tuple<NodeController, NodeController> ParentChildTuple { get; set; }
    public Tuple<NodeController, NodeController> ChildParentTuple { get; set; }
    public List<NodeController> RelatedNodes { get; set; }

    public LinkRelation(int relationId, Tuple<NodeController, NodeController> parentChildTuple, Tuple<NodeController, NodeController> childParentTuple)
    {
        RelationId = relationId;
        ParentChildTuple = parentChildTuple;
        ChildParentTuple = childParentTuple;
    }
}
