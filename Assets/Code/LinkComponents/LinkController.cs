﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

//https://blogs.unity3d.com/2018/10/05/art-that-moves-creating-animated-materials-with-shader-graph/

public class LinkController : MonoBehaviour
{
    #region Data
    [Header("Data")]
    [SerializeField] private LinkData linkData;
    public LinkData LinkData { get => linkData; set => linkData = value; }
    #endregion

    #region Components
    [Header("Components")]
    [SerializeField] private LineRenderer linkLine;
    public LineRenderer LinkLine { get => linkLine; set => linkLine = value; }

    [SerializeField] private GameObject arrowSprite;
    public GameObject ArrowSprite { get => arrowSprite; set => arrowSprite = value; }

    [SerializeField] private GameObject linkLabel;
    public GameObject LinkLabel { get => linkLabel; set => linkLabel = value; }

    [SerializeField] private TMP_InputField linkLabel_InputField;
    public TMP_InputField LinkLabel_InputField { get => linkLabel_InputField; set => linkLabel_InputField = value; }

    [SerializeField] private LinkDataPanel linkDataPanel;
    public LinkDataPanel LinkDataPanel { get => linkDataPanel; set => linkDataPanel = value; }

    #endregion

    #region References
    [Header("Components")]
    [SerializeField] private NodeController parentNode;
    public NodeController ParentNode { get => parentNode; private set => parentNode = value; }

    [SerializeField] private NodeController childNode;
    public NodeController ChildNode { get => childNode; private set => childNode = value; }

    public Tuple<NodeController, NodeController> NodeRelation { get; set; }

    public int TotalRelatedLinks;
    public int relationIndex;
    public int magnifier;
    #endregion
    public OnDeleteLink onDelete;
    private void Awake()
    {
        if (linkData == null)
        {
            linkData = new LinkData();
            linkData.NodeLinkId = IDGenerator.GenerateID();
        }
        linkData.AssignedLinkController = this;
    }
    private void Start()
    {
        linkLabel_InputField.onValueChanged.AddListener(SetLinkText);
    }

    #region Parent
    private void AssignParent(NodeController parentNode)
    {
        //if (ParentNode != null && ParentNode != parentNode)
        //{
        //    UnassignParent();
        //}
        if (ParentNode != null)
        {
            UnassignParent();
        }
        linkData.ParentController = parentNode;
        linkData.ParentNodeId = parentNode.NodeId;

        ParentNode = parentNode;

        //ParentNode.NodeData.NodeLinks.Add(linkData);
        ParentNode.AddRelatedLink(this);
        ParentNode.onDelete.AddListener(DeleteLink);
        SetLineParentPoint(ParentNode.transform.position);
        ParentNode.DragMovement.onPositionChanged.AddListener(SetLineParentPoint); 
        //SetLineLabelPoint();
    }
    private void UnassignParent()
    {
        if (ParentNode != null)
        {
            ParentNode.RemoveRelatedLink(this);

            ParentNode.onDelete.RemoveListener(DeleteLink);
            ParentNode.DragMovement.onPositionChanged.RemoveListener(SetLineParentPoint);

            linkData.ParentController = null;
            linkData.ParentNodeId = "NONE";
            ParentNode = null;
        }
        SetLineLabelPoint();
    }
    #endregion

    #region Child
    public void AssignChild(NodeController childNode)
    {
        if (ChildNode != null)
        {
            UnassignChild();
        }

        linkData.ChildController = childNode;
        linkData.ChildNodeId = childNode.NodeId;
        ChildNode = childNode;

        ChildNode.AddRelatedLink(this);

        ChildNode.onDelete.AddListener(DeleteLink);
        ChildNode.DragMovement.onPositionChanged.AddListener(SetLineChildPoint);

        SetLineChildPoint(ChildNode.transform.position);
    }
    private void UnassignChild()
    {
        if (ChildNode != null)
        {
            ChildNode.RemoveRelatedLink(this);
            ChildNode.onDelete.RemoveListener(DeleteLink);
            ChildNode.DragMovement.onPositionChanged.RemoveListener(SetLineChildPoint);
            linkData.ChildController = null;
            linkData.ChildNodeId = "NONE";
            ChildNode = null;
        }

        SetLineLabelPoint();
    }

    public void ChangeChildTarget(NodeController childTarget)
    {
        AssignChild(childTarget);
    }

    #endregion

    #region Parent and Child
    public void AssignParentAndChild(NodeController parentNode, NodeController childNode)
    {
        if (ParentNode != null && ParentNode != parentNode)
        {
            UnassignParent();
        }

        NodeRelation = new Tuple<NodeController, NodeController>(parentNode, childNode);

        AssignParent(parentNode);
        AssignChild(childNode);
    }
    #endregion

    #region Line
    public void SetLineParentPoint(Vector2 pos)
    {
        LinkLine.SetPosition(0, pos);
        SetLineLabelPoint();
    }
    public void SetLineChildPoint(Vector2 pos)
    {
        LinkLine.SetPosition(2, pos);
        SetLineLabelPoint();
    }

    public void SetLineColor(Material mat)
    {
        LinkLine.material = mat;
    }

    public void SetLineLabelPoint()
    {
        float resultoffset = 0;
        if (TotalRelatedLinks > 1)
        {
            float offset = magnifier * relationIndex;
            float centeroffset = magnifier * TotalRelatedLinks * 0.5f;
            resultoffset = offset - centeroffset;
        }

        Vector2 crossVector = VectorMathUtility.CrossProduct(LinkLine.GetPosition(0), LinkLine.GetPosition(2)).normalized;
        if (crossVector.y < 0)
        {
            crossVector = VectorMathUtility.CrossProduct(LinkLine.GetPosition(2), LinkLine.GetPosition(0)).normalized;
            Vector2 labelpos = VectorMathUtility.MidPointBetweenVector2(LinkLine.GetPosition(2), LinkLine.GetPosition(0)) + crossVector * resultoffset;
            SetLinePointPosition(1, labelpos);
            linkLabel.gameObject.transform.position = labelpos;
        }
        else
        {
            Vector2 labelpos = VectorMathUtility.MidPointBetweenVector2(LinkLine.GetPosition(0), LinkLine.GetPosition(2)) + crossVector * resultoffset;
            SetLinePointPosition(1, labelpos);
            linkLabel.gameObject.transform.position = labelpos;
        }
        SetArrowPositionRotation();
    }

    public void SetLinePointPosition(int pointIndex, Vector2 position)
    {
        LinkLine.SetPosition(pointIndex, position);
    }
    #endregion

    #region Label And Arrow
    private void SetLinkText(string value)
    {
        LinkData.LinkText = value;

        if (NodeViewManager.Instance.SelectedController == parentNode)
        {
            if(LinkDataPanel != null)
            {
                LinkDataPanel.LinkText_InputField.text = value;
            }
        }
    }
    public void SetArrowPositionRotation()
    {
        ArrowSprite.transform.position = VectorMathUtility.MidPointBetweenVector2(linkLine.GetPosition(1), linkLine.GetPosition(2));
        var direction = linkLine.GetPosition(2) - linkLine.GetPosition(1);
        var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        ArrowSprite.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    #endregion
    #region Delete
    public void DeleteLink()
    {
        //Debug.Log("Link_" + LinkData.NodeLinkId + " is being deleted.");
        NodeController temp = ParentNode;
        if (ParentNode)
        {
            UnassignParent();
            if (NodeViewManager.Instance.SelectedController == temp)
            {
                NodeViewManager.Instance.DisplayPanel(temp);
            }
        }
        temp = ChildNode;
        if (ChildNode)
        {
            UnassignChild();
            if (NodeViewManager.Instance.SelectedController == temp)
            {
                NodeViewManager.Instance.DisplayPanel(temp);
            }
        }
        onDelete.Invoke(this);
        StartCoroutine(SafeDeleteRoutine());
    }

    IEnumerator SafeDeleteRoutine()
    {
        yield return null; // Yield for 1 frame.
        DebugOutput.Instance.Log("NodeLink " + gameObject.name + " Destroyed");
        Destroy(gameObject);
    }

    //private void OnDestroy()
    //{
    //    Debug.Log("NodeLink " + gameObject.name + " Destroyed");
    //}
    #endregion
}
