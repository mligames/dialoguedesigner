﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Color Swatch", fileName = "newColorSwatch")]
public class ColorSwatch : ScriptableObject
{
    [Header("Color Name")]
    public string colorName;

    [Header("RGBA Values")]
    [Range(0, 255)]
    public int red;
    [Range(0, 255)]
    public int green;
    [Range(0, 255)]
    public int blue;
    [Range(0, 255)]
    public int alpha = 255;

    [Header("Hex Value")]
    public string hex;

    public void ApplyToImage(Image image)
    {
        image.color = new Color(red, blue, green, alpha);
    }
    public void ApplyToText(Image image)
    {
        image.color = new Color(red, blue, green, alpha);
    }
}
