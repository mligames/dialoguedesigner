﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Color Palette", fileName = "newColorPalette")]
public class ColorPalette : ScriptableObject
{
    public string paletteName;
    public ColorSwatch[] colorSwatches;
}
