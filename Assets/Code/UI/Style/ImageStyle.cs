﻿using UnityEngine;
using UnityEngine.UI;

public class ImageStyle : MonoBehaviour
{
    [SerializeField] private Image image;
    public ColorSwatch colorSwatch;

    private void Start()
    {
        if (colorSwatch != null)
        {
            SetImageColor(colorSwatch.hex);
        }
    }

    public void SetImageColor(string value)
    {
        if (image == null)
        {
            image = GetComponent<Image>();
        }

        if (image != null)
        {
            string colorValue = "#" + value;
            if (ColorUtility.TryParseHtmlString(colorValue, out Color color))
            {
                image.color = color;
            }
        }

    }
#if UNITY_EDITOR
    private void OnValidate()
    {
        if (colorSwatch != null)
        {
            SetImageColor(colorSwatch.hex);
        }
    }
#endif
}
