﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
//[Serializable] public enum NodeCreationState { INPROGRESS, }
public class ClickDragCreateNode : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler
{
    public GameObject DummyNodePrefab;
    public NodeController NodeControllerPrefab;
    public GameObject DummyNode;
    public void OnPointerEnter(PointerEventData eventData)
    {
        UserCamera.Instance.IsPanningBlocked = true;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        DummyNode = Instantiate<GameObject>(DummyNodePrefab, null);
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        DummyNode.transform.position = mousePos;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (DummyNode != null)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            DummyNode.transform.position = mousePos;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (DummyNode != null)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            DummyNode.transform.position = mousePos;

            DesignerManager.Instance.CreateNodeController(mousePos);

            Destroy(DummyNode.gameObject);
            DummyNode = null;
        }
        UserCamera.Instance.IsPanningBlocked = false;
    }


}
