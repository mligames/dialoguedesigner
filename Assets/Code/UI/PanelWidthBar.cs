﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PanelWidthBar : MonoBehaviour, IPointerDownHandler, IDragHandler
{
	public bool InvertDragMovment;
	public float MinWidthSize = 300.0f;
	public float MaxWidthSize = 1000.0f;

	public RectTransform PanelRectTransform;
	private Vector2 originalLocalPointerPosition;
	private Vector2 originalSizeDelta;

    private void Start()
    {
        if (PanelRectTransform)
        {
			Vector2 sizeDelta = PanelRectTransform.sizeDelta;
			PanelRectTransform.sizeDelta = new Vector2(Mathf.Clamp(sizeDelta.x, MinWidthSize, MaxWidthSize), sizeDelta.y);
		}			

	}
    public void OnPointerDown(PointerEventData data)
	{
		originalSizeDelta = PanelRectTransform.sizeDelta;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(PanelRectTransform, data.position, data.pressEventCamera, out originalLocalPointerPosition);
	}

	public void OnDrag(PointerEventData data)
	{
		if (PanelRectTransform == null)
			return;

		Vector2 localPointerPosition;
		Vector2 sizeDelta;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(PanelRectTransform, data.position, data.pressEventCamera, out localPointerPosition);
		Vector3 offsetToOriginal = localPointerPosition - originalLocalPointerPosition;
		
		if (InvertDragMovment)
		{
			sizeDelta = originalSizeDelta + new Vector2(-offsetToOriginal.x, 0.0f);
			sizeDelta = new Vector2(
				Mathf.Clamp(sizeDelta.x, MinWidthSize, MaxWidthSize), sizeDelta.y
			);
		}
		else
		{
			sizeDelta = originalSizeDelta + new Vector2(offsetToOriginal.x, 0.0f);
			sizeDelta = new Vector2(
				Mathf.Clamp(sizeDelta.x, MinWidthSize, MaxWidthSize), sizeDelta.y
			);
		}

		PanelRectTransform.sizeDelta = sizeDelta;
	}
}
