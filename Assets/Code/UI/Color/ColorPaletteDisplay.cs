﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPaletteDisplay : MonoBehaviour
{
    public ColorPalette ColorPalette;
    public Image[] displayImages;
    // Start is called before the first frame update
    void Start()
    {
        LoadColors();
    }
    /// <summary>
    /// [Refactor Note] Need to make this safer
    /// </summary>
    public void LoadColors()
    {
        for (int i = 0; i < displayImages.Length; i++)
        {
            string colorValue = "#" + ColorPalette.colorSwatches[i].hex;
            if (ColorUtility.TryParseHtmlString(colorValue, out Color color))
            {
                displayImages[i].color = color;
            }
        }
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        LoadColors();
    }
#endif
}
