﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public struct ColorInfo
{
    public readonly string colorName;
    public readonly RGBAInfo rgba;
    public readonly string hex;

    public ColorInfo(string colorName, RGBAInfo rgba, string hex)
    {
        this.colorName = colorName;
        this.rgba = rgba;
        this.hex = hex;
    }

}
