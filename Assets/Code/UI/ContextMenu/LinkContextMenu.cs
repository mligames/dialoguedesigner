﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LinkContextMenu : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public LinkController SelectedLink;

    public Button DeleteLinkButton;
    public Vector2 MouseWorldPoint
    {
        get
        {
            return Camera.main.ScreenToWorldPoint(
                new Vector3(Input.mousePosition.x,
                Input.mousePosition.y,
                Camera.main.nearClipPlane));
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;
        DeleteLinkButton.onClick.AddListener(DeleteLink);
    }

    public void ActivateMenu(LinkController nodeLink)
    {
        gameObject.SetActive(true);
        gameObject.transform.position = MouseWorldPoint;

        SelectedLink = nodeLink;
    }

    public void CloseMenu()
    {
        gameObject.SetActive(false);
    }

    public void DeleteLink()
    {
        Debug.Log("Delete Link");
        SelectedLink.DeleteLink();
        SelectedLink = null;
        CloseMenu();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //UserInput.Instance.isOverContextMenu = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //UserInput.Instance.isOverContextMenu = false;
    }

    private void OnDisable()
    {
        //UserInput.Instance.isOverContextMenu = false;
    }
}
