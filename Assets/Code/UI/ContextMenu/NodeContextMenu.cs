﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NodeContextMenu : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public NodeController SelectedNode;

    //public TMP_Text Text_SelectedNodeName;

    public Button CopyNodeButton;
    public Button DeleteNodeButton;
    public Vector2 MouseWorldPoint
    {
        get
        {
            return Camera.main.ScreenToWorldPoint(
                new Vector3(Input.mousePosition.x,
                Input.mousePosition.y,
                Camera.main.nearClipPlane));
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Canvas>().worldCamera = Camera.main;
        CopyNodeButton.onClick.AddListener(CreateNodeCopy);
        DeleteNodeButton.onClick.AddListener(DeleteNode);
        //if (Text_SelectedNodeName == null)
        //    Text_SelectedNodeName = GetComponentInChildren<TMP_Text>();
    }

    public void ActivateMenu(NodeController nodeController)
    {
        gameObject.SetActive(true);
        gameObject.transform.position = MouseWorldPoint;

        SelectedNode = nodeController;

        //Text_SelectedNodeName.SetText(nodeController.NodeTextComp.TextComp.text);
    }

    public void CloseMenu()
    {
        gameObject.SetActive(false);
    }

    public void CreateNodeCopy()
    {
        Debug.Log("TODO: REFACTOR COPY NODE AND NODE FACTORY");

        //if (SelectedNode == null)
        //    return;

        //NodeController nodeCopy = Instantiate(SelectedNode, null);

        //Debug.Log("Selected Node Data: " + SelectedNode.NodeData.NodeType);

        //switch (SelectedNode.NodeData.NodeType)
        //{
        //    case NodeType.NARRATIVE:
        //        nodeCopy.NodeData = ResourceFactory.CopyNarrativeNodeData((NarrativeNodeData)SelectedNode.NodeData);
        //        break;
        //    case NodeType.MCQ:
        //        nodeCopy.NodeData = ResourceFactory.CopyMCQNodeData((MCQNodeData)SelectedNode.NodeData);
        //        break;
        //    case NodeType.BRANCHING:
        //        nodeCopy.NodeData = ResourceFactory.CopyBranchingNodeData((BranchingNodeData)SelectedNode.NodeData);
        //        break;
        //    case NodeType.RANDOM_ROUTE:
        //        break;
        //    case NodeType.LOGIC:
        //        break;
        //    case NodeType.INQUIRY:
        //        nodeCopy.NodeData = ResourceFactory.CopyInquiryNodeData((InquiryNodeData)SelectedNode.NodeData);
        //        break;
        //    case NodeType.TEXT_RESPONSE:
        //        nodeCopy.NodeData = ResourceFactory.CopyTextResponseNodeData((TextResponseNodeData)SelectedNode.NodeData);
        //        break;
        //    case NodeType.UNDEFINED:
        //        break;
        //    default:
        //        Debug.Log("Default switch case hit... This shouldn't have happened.");
        //        break;
        //}
        //nodeCopy.NodeData.NodeId = "TEST-Node-" + DateTime.UtcNow.ToBinary();
        //nodeCopy.transform.position = SelectedNode.transform.position + new Vector3(3,0,0);
        //nodeCopy.PostNodeDataAPI();
        //SelectedNode = null;
        //CloseMenu();
    }
    public void DeleteNode()
    {
        Debug.Log("Delete Node");
        //SelectedNode.OnNodeDelete();
        //SelectedNode = null;
        //CloseMenu();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //UserInput.Instance.isOverContextMenu = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //UserInput.Instance.isOverContextMenu = false;
    }

    private void OnDisable()
    {
        //UserInput.Instance.isOverContextMenu = false;
    }
}
