﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICanvasRoot : MonoBehaviour
{
    public Canvas CanvasRoot;
    public RectTransform ContentRoot;

}
