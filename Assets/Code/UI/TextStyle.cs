﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TextStyle : MonoBehaviour
{
    public ColorSwatch colorSwatch;

    private void Start()
    {
        if (colorSwatch != null)
        {
            SetTextColor(colorSwatch.hex);
        }
    }

    public void SetTextColor(string value)
    {
        TMP_Text textComp = GetComponent<TMP_Text>();
        if (textComp != null)
        {
            string colorValue = "#" + value;
            if (ColorUtility.TryParseHtmlString(colorValue, out Color color))
            {
                textComp.color = color;
            }
        }

    }
#if UNITY_EDITOR
    private void OnValidate()
    {
        if (colorSwatch != null)
        {
            SetTextColor(colorSwatch.hex);
        }
    }
#endif
}
