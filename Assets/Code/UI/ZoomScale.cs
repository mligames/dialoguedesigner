﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class ZoomScale : Selectable
{
    public Scrollbar Scrollbar;
    public TMP_Text TextComp;

    public float minZoom = 100f;
    public float maxZoom = 600f;
    public float MidZoom { get => ((maxZoom - minZoom) * 0.5f) + 100; }

    public float DistanceRangeValue { get { return maxZoom - minZoom; } }

    public float currentZoomValue;
    public float zoomPercentValue;
    public float CameraZoomValue { get => Camera.main.orthographicSize; }
    //public float CameraZoomPercent { get => (Camera.main.orthographicSize - minZoom) / DistanceRangeValue; }
    public float CameraZoomPercent { get =>  DistanceRangeValue / (Camera.main.orthographicSize - minZoom); }

    public float mouseScrollValue;

    public OnOverUI onOverZoomScale;
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        Scrollbar.onValueChanged.AddListener(OnScrollHandleValueChange);
        Scrollbar.value = 0.5f;

        Camera.main.orthographicSize = PercentToValue(0.5f);
        
    }

    // Update is called once per frame
    void Update()
    {
        //currentZoomValue = CameraZoomValue;
        //zoomPercentValue = CameraZoomPercent;
        if (Input.mouseScrollDelta.y != 0)
        {
            MouseCameraZoom(-Input.mouseScrollDelta.y);
            SetScrollValue(Camera.main.orthographicSize);
        }
    }
    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        if (!UserCamera.Instance.IsPanning)
        {
            UserCamera.Instance.IsPanningBlocked = true;
        }
    }
    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
        if (!UserCamera.Instance.IsPanning)
        {
            UserCamera.Instance.IsPanningBlocked = false;
        }
    }

    private void MouseCameraZoom(float mouseDelta)
    {
        mouseScrollValue = mouseDelta *= 10;
        float scrollValue = Camera.main.orthographicSize + mouseDelta;

        float zoom = Mathf.Clamp(scrollValue, minZoom, maxZoom);
        Camera.main.orthographicSize = zoom;
        //SetScrollTextValue(Scrollbar.value);
    }

    private void SetScrollValue(float value)
    {
        Scrollbar.value = (value - minZoom) / DistanceRangeValue;
        SetScrollTextValue(Scrollbar.value);
    }
    private void SetScrollTextValue(float value)
    {
        //float percent = 1.0f - value;
        TextComp.text = "" + (int)((1.0f - value) * 200f) + " %";
    }
    private void OnScrollHandleValueChange(float value)
    {
        Camera.main.orthographicSize = PercentToValue(value);
        SetScrollTextValue(value);
    }
    private float PercentToValue(float percent)
    {
        return (DistanceRangeValue * percent) + 100;
    }
}
