﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Collider2D))]
public class CreateLinkOnDrag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    #region Prefabs
    public DummyLinkLine DummyLinkPrefab;
    #endregion

    #region Components
    [Header("Components")]
    [SerializeField] private Collider2D colliderComp;
    public Collider2D CollierComp { get => colliderComp; set => colliderComp = value; }
    #endregion

    #region References
    [Header("References")]
    [SerializeField] private NodeController parentController;
    public NodeController ParentController { get => parentController; set => parentController = value; }
    #endregion

    public DummyLinkLine DummyLink;
    public OnBeginDrag onLinkDragBegin;
    public OnEndDrag onLinkDragEnd;

    private void Start()
    {
        parentController = transform.root.GetComponent<NodeController>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (DummyLink != null)
        {
            Destroy(DummyLink.gameObject);
            DummyLink = Instantiate<DummyLinkLine>(DummyLinkPrefab, null);
        }

        if (DesignerManager.Instance.NodeControllerOverMouse(out NodeController node))
        {
            //DebugOutput.Instance.Log("Begin Node Link Creation " + node.name);
            Debug.Log("Begin Node Link Creation " + node.name);

            parentController = node;
            if (parentController.NodeData.NodeType == NodeType.BRANCHING || parentController.NodeData.NodeLinks.Count < 1)
            {
                DesignerManager.Instance.CreateNewLinkController(parentController, node);
            }
            else if (parentController.NodeData.NodeType == NodeType.NARRATIVE && parentController.NodeData.NodeLinks.Count > 0)
            {
                parentController.NodeData.NodeType = NodeType.BRANCHING;
                DesignerManager.Instance.CreateNewLinkController(parentController, node);
                Debug.Log("Narrative Node changed to Branch Node");
            }
            else
            {
                Debug.Log("TODO: ADD WARNING!");
            }
        }


        DummyLink = Instantiate<DummyLinkLine>(DummyLinkPrefab, null);
        DummyLink.LineRenderer.SetPosition(0, parentController.transform.position);

        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        DummyLink.LineRenderer.SetPosition(1, mousePos);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (DummyLink != null)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            DummyLink.LineRenderer.SetPosition(1, mousePos);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (DummyLink != null)
        {
            Destroy(DummyLink.gameObject);
            DummyLink = null;
        }

        if (DesignerManager.Instance.NodeControllerOverMouse(out NodeController childController))
        {
            if(childController != parentController)
            {                
                DesignerManager.Instance.CreateNewLinkController(ParentController, childController);
                Debug.Log("Connected " + parentController.NodeTitle + " to " + childController.NodeTitle);

            }
            else
            {
                Debug.Log("Cannot have a connection between the same node.");
            }
        }
        else
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos.z = 0;

            childController = DesignerManager.Instance.CreateNodeController(pos);
            Debug.Log("Created " + childController.NodeTitle + " to " + childController.NodeTitle);
            DesignerManager.Instance.CreateNewLinkController(ParentController, childController);
            Debug.Log("Connected " + parentController.NodeTitle + " to " + childController.NodeTitle);
        }       
    }
}
