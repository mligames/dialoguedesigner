﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

[Serializable] public enum AppMode { ONLINE, ONLINE_TEST, LOCAL_TEST};

[RequireComponent(typeof(APIController))]
public class DesignerManager : Singleton<DesignerManager>
{
    #region Prefabs
    [Header("Application Mode")]
    [SerializeField] AppMode appMode;
    public AppMode AppMode { get => appMode; private set => appMode = value; }
    #endregion

    #region Prefabs
    [Header("Prefab")]
    [SerializeField] NodeController nodeControllerPrefab;
    public NodeController NodeControllerPrefab { get => nodeControllerPrefab; private set => nodeControllerPrefab = value; }

    [SerializeField] LinkController linkControllerPrefab;
    public LinkController LinkControllerPrefab { get => linkControllerPrefab; private set => linkControllerPrefab = value; }
    #endregion

    public Vector2 MouseWorldPoint
    {
        get
        {
            return Camera.main.ScreenToWorldPoint(
                new Vector3(Input.mousePosition.x,
                Input.mousePosition.y,
                Camera.main.nearClipPlane));
        }
    }

    public Dictionary<string, NodeController> NodeDictionary;
    public Dictionary<string, LinkController> LinkDictionary;

    [SerializeField] bool loadLocalTestData;
    public bool LoadLocalTestdata { get => loadLocalTestData; private set => loadLocalTestData = value; }

    [SerializeField] TextAsset localTestData;
    public TextAsset LocalTestData { get => localTestData; private set => localTestData = value; }

    [SerializeField] TextAsset localTestQuestionData;
    public TextAsset LocalTestQuestionData { get => localTestQuestionData; private set => localTestQuestionData = value; }

    [Header("CaseData")]
    [SerializeField] CaseData caseData;
    public CaseData CaseData { get => caseData; private set => caseData = value; }

    //[SerializeField] CaseQuestionData caseQuestionData;
    //public CaseQuestionData CaseQuestionData { get => caseQuestionData; private set => caseQuestionData = value; }

    [SerializeField] NodeController currentSelected;
    public NodeController CurrentSelected { get => currentSelected; set => currentSelected = value; }

    [Header("API")]
    [SerializeField] APIController apiController;
    public APIController ApiController { get => apiController; private set => apiController = value; }


    #region Monobehaviour

    protected override void Awake()
    {
        base.Awake();
        NodeDictionary = new Dictionary<string, NodeController>();
        LinkDictionary = new Dictionary<string, LinkController>();
    }
    // Start is called before the first frame update
    void Start()
    {
        if(apiController == null)
        {
            apiController = GetComponent<APIController>();
            apiController.onCaseDataLoaded.AddListener(OnCaseDataLoaded);
            apiController.onCaseQuestionsLoaded.AddListener(OnCaseQuestionsLoaded);
        }

        //CaseQuestionData = ScriptableObject.CreateInstance<CaseQuestionData>();

        switch (appMode)
        {
            case AppMode.ONLINE:

                break;
            case AppMode.ONLINE_TEST:
                apiController.GetRequestCaseData(apiController.CaseDataUrl(apiController.DevelopmentTest));
                //apiController.GetRequestCaseQuestionsData(apiController.CaseDataUrl(apiController.DevelopmentTest));
                break;
            case AppMode.LOCAL_TEST:
                apiController.GetLocalJsonCaseData();
                apiController.GetLocalJsonCaseQuestionDataData();
                break;
        }
    }
    #endregion
    public bool NodeControllerOverMouse(out NodeController node)
    {
        RaycastHit2D hit = Physics2D.Raycast(MouseWorldPoint, Vector2.zero, 0f);
        if (hit)
        {
            NodeController nodeController = hit.collider.GetComponentInParent<NodeController>();
            if (nodeController != null)
            {
                node = nodeController;
                return true;
            }
            else
            {
                node = null;
                return false;
            }
        }
        else
        {
            node = null;
            return false;
        }

    }
    private void OnCaseDataLoaded(bool successfulLoad, CaseData caseData)
    {
        if (successfulLoad)
        {
            DebugOutput.Instance.Log("Load CaseData from API SUCCESS");
            CaseData = caseData;
            if(CaseData != null)
            {
                DebugOutput.Instance.Log("CaseData is NULL");
            }
            LoadMapFromCaseData(CaseData);
        }
        else
        {
            DebugOutput.Instance.Log("Load CaseData from API FALED!");
            CaseData = ScriptableObject.CreateInstance<CaseData>();
            CaseData.caseId = "TEST-CASE-" + DateTime.UtcNow.ToBinary();
        }
    }

    private void LoadMapFromCaseData(CaseData caseData)
    {
        NodeDictionary = new Dictionary<string, NodeController>();

        if (caseData != null)
        {
            if (caseData.nodeList.Count > 0)
            {
                foreach (var item in caseData.nodeList)
                {
                    NodeController nodeController = CreateNodeController(item);

                    if (nodeController != null)
                    {
                        if (nodeController.NodeId != null || nodeController.NodeId != "")
                        {
                            string nodeControllerId = nodeController.NodeId;

                            if (!NodeDictionary.ContainsKey(nodeControllerId))
                            {
                                NodeDictionary.Add(nodeControllerId, nodeController);
                            }
                            else
                            {
                                Debug.Log("NodeController " + nodeController.NodeId + "was already added!");
                            }
                        }
                    }
                }

                foreach (NodeController nodeController in NodeDictionary.Values)
                {
                    foreach (LinkData linkItem in nodeController.NodeData.NodeLinks)
                    {
                        CreateLinkController(linkItem);
                    }
                }

                apiController.GetRequestCaseQuestionsData(apiController.CaseDataUrl(apiController.DevelopmentTest));
            }
        }
    }

    private void OnCaseQuestionsLoaded(bool successfulLoad, CaseQuestionsAPIWrapper questionsData)
    {
        if (successfulLoad)
        {
            DebugOutput.Instance.Log("Load CaseData Questions from API SUCCESS");
            foreach (QuestionData questionData in questionsData.AllQuestionData)
            {
                if (NodeDictionary.TryGetValue(questionData.NodeId, out NodeController nodeController))
                {
                    nodeController.NodeData.QuestionData = questionData;
                    //Debug.Log("Question Found it Node!");
                }
                else
                {
                    Debug.Log("Question Did not find its Node!");
                }
            }
        }
        else
        {
            DebugOutput.Instance.Log("Load CaseData Questions from API FAILED");
        }
    }

    #region Controllers
    private NodeController CreateNodeController()
    {
        return GameObject.Instantiate(nodeControllerPrefab);
    }
    public NodeController CreateNodeController(Vector2 pos)
    {
        NodeController nodeController = CreateNodeController();
        nodeController.NodeData = new NodeData(IDGenerator.GenerateID());
        nodeController.NodeData.CaseId = DesignerManager.Instance.CaseData.caseId;
        nodeController.transform.position = pos;
        nodeController.name = "Node_" + nodeController.NodeData.NodeId;
        AddToNodeDictionary(nodeController);
        return nodeController;
    }
    private NodeController CreateNodeController(NodeData nodeData)
    {
        NodeController nodeController = CreateNodeController();
        nodeController.NodeData = nodeData;
        //nodeController.NodeData.CaseId = DesignerManager.Instance.CaseData.caseId;
        nodeController.ChangeNodeTitle(nodeData.NodeTitle);
        nodeController.transform.position = new Vector2(nodeData.NodeCoordinates.NodeX, nodeData.NodeCoordinates.NodeY);
        nodeController.name = "Node_" + nodeController.NodeData.NodeId;

        AddToNodeDictionary(nodeController);
        return nodeController;
    }
    public void DeleteNodeController(NodeController nodeController)
    {
        DebugOutput.Instance.Log("Deleting... " + nodeController.NodeData.NodeId);
        RemoveFromNodeDictionary(nodeController.NodeData.NodeId);
        //nodeController.DE();
    }
    public bool AddToNodeDictionary(NodeController nodeController)
    {
        if (nodeController.NodeData == null)
        {
            if (!NodeDictionary.ContainsKey(nodeController.NodeData.NodeId))
            {
                NodeDictionary.Add(nodeController.NodeData.NodeId, nodeController);
                return true;
            }
        }
        return false;
    }
    public NodeController RemoveFromNodeDictionary(string nodeId)
    {
        if (NodeDictionary.TryGetValue(nodeId, out NodeController nodeController))
        {
            return nodeController;
        }
        return null;
    }

    public NodeController FindNodeController(string nodeId)
    {
        if(NodeDictionary.TryGetValue(nodeId, out NodeController nodeController))
        {
            return nodeController;
        }
        return null;
    }

    #endregion

    #region Links

    private LinkController CreateLinkController()
    {
        //NodeController node = GameObject.Instantiate(nodeControllerPrefab);
        return GameObject.Instantiate(linkControllerPrefab);
    }
    public LinkData CreateLinkData()
    {
        LinkData linkData = new LinkData(IDGenerator.GenerateID());
        //linkData.NodeLinkId = IDGenerator.GenerateID();
        return linkData;
    }
    public void CreateNewLinkController(NodeController parentNode, NodeController childNode)
    {
        DebugOutput.Instance.Log("Parent " + parentNode.name + " Connected To Child: " + childNode.name);
        Debug.Log("Parent " + parentNode.name + " Connected To Child: " + childNode.name);
        LinkController linkController = CreateLinkController();
        linkController.LinkData = CreateLinkData();
        linkController.LinkData.AssignedLinkController = linkController;

        linkController.AssignParentAndChild(parentNode, childNode);

        linkController.LinkLabel_InputField.text = linkController.LinkData.LinkText;

        AddToLinkDictionary(linkController);

        linkController.name = "Link_" + linkController.LinkData.NodeLinkId;

        if (NodeViewManager.Instance.SelectedController == parentNode)
        {
            NodeViewManager.Instance.DeactivateAllPanels();
            NodeViewManager.Instance.DisplayPanel(parentNode);
        }

        UpdateRelationCountBetween(linkController.ParentNode, linkController.ChildNode);
    }
    public void CreateLinkController(LinkData linkData)
    {
        //Debug.Log("Creating LinkController from LinkData...");

        LinkController linkController = GameObject.Instantiate(linkControllerPrefab);
        linkController.LinkData = linkData;

        if(NodeDictionary.TryGetValue(linkController.LinkData.ParentNodeId, out NodeController parentController))
        {
            if (NodeDictionary.TryGetValue(linkController.LinkData.ChildNodeId, out NodeController childController))
            {
                linkController.AssignParentAndChild(parentController, childController);
                linkController.LinkData.LinkTargetInfo = parentController.NodeData.NodeTitle + " -> " + childController.NodeData.NodeTitle;
                linkController.LinkData.AssignedLinkController = linkController;
                linkController.LinkLabel_InputField.text = linkController.LinkData.LinkText;
                linkController.name = "Link_" + linkController.LinkData.NodeLinkId;
                AddToLinkDictionary(linkController);

                if (NodeViewManager.Instance.SelectedController == parentController)
                {
                    NodeViewManager.Instance.DeactivateAllPanels();
                    NodeViewManager.Instance.DisplayPanel(parentController);
                }

                UpdateRelationCountBetween(linkController.ParentNode, linkController.ChildNode);
            }
        }
        else
        {
            linkController.DeleteLink();
            Debug.Log("[-------ERROR CREATING LINK FROM LINK DATA!---------]");
        }
    }

    //public void CreateLinkController(LinkData linkData, NodeController parentController, NodeController childController)
    //{
    //    DebugOutput.Instance.Log("Creating LinkController from LinkModelData...");
    //    Debug.Log("Creating LinkController from LinkModelData...");

    //    LinkController linkController = GameObject.Instantiate(linkControllerPrefab);
    //    linkController.LinkData = new LinkData(linkData);
    //    linkController.LinkData.LinkTargetInfo = parentController.NodeData.NodeTitle + " -> " + childController.NodeData.NodeTitle;
    //    //linkController.LinkData.ParentController = parentController;
    //    //linkController.LinkData.ChildController = childController;
    //    linkController.LinkData.ParentChildRelation = new Tuple<NodeController, NodeController>(parentController, childController);

    //    linkController.LinkData.AssignedLinkController = linkController;
    //    linkController.AssignParentAndChild(parentController, childController);

    //    linkController.LinkLabel_InputField.text = linkController.LinkData.LinkText;

    //    AddToLinkDictionary(linkController);

    //    linkController.name = "Link_" + linkController.LinkData.NodeLinkId;
    //    if (NodeViewManager.Instance.SelectedController == parentController)
    //    {
    //        NodeViewManager.Instance.DeactivateAllPanels();
    //        NodeViewManager.Instance.DisplayPanel(parentController);
    //    }

    //    UpdateRelationCountBetween(linkController.ParentNode, linkController.ChildNode);
    //}
    //public void CreateLinkController(LinkDataModel linkDataModel, NodeController parentController, NodeController childController)
    //{
    //    DebugOutput.Instance.Log("Creating LinkController from LinkModelData...");    
    //    Debug.Log("Creating LinkController from LinkModelData...");

    //    LinkController linkController = GameObject.Instantiate(linkControllerPrefab);
    //    linkController.LinkData = NodeDataFactory.CreateLinkData(linkDataModel);
    //    linkController.LinkData.LinkTargetInfo = parentController.NodeData.NodeTitle + " -> " + childController.NodeData.NodeTitle;
    //    //linkController.LinkData.ParentController = parentController;
    //    //linkController.LinkData.ChildController = childController;
    //    linkController.LinkData.ParentChildRelation = new Tuple<NodeController, NodeController>(parentController, childController);

    //    linkController.LinkData.LinkController = linkController;
    //    linkController.AssignParentAndChild(parentController, childController);

    //    linkController.LinkLabel_InputField.text = linkController.LinkData.LinkText;

    //    AddToLinkDictionary(linkController);

    //    linkController.name = "Link_" + linkController.LinkData.NodeLinkId;
    //    if (NodeViewManager.Instance.SelectedController == parentController)
    //    {
    //        NodeViewManager.Instance.DeactivateAllPanels();
    //        NodeViewManager.Instance.DisplayPanel(parentController);
    //    }

    //    UpdateRelationCountBetween(linkController.ParentNode, linkController.ChildNode);
    //}

    public void DeleteLinkController(LinkController linkController)
    {
        DebugOutput.Instance.Log("Deleting... " + linkController.LinkData.NodeLinkId);
        RemoveFromLinkDictionary(linkController);
        linkController.DeleteLink();
    }

    public void AddToLinkDictionary(LinkController linkController)
    {
        if (!LinkDictionary.ContainsKey(linkController.LinkData.NodeLinkId))
        {
            LinkDictionary.Add(linkController.LinkData.NodeLinkId, linkController);
        }
        else
        {
            Debug.Log(linkController.LinkData.NodeLinkId + " was already added to the link dictionary.");
        }
    }

    public void RemoveFromLinkDictionary(LinkController linkController)
    {
        if (LinkDictionary.ContainsKey(linkController.LinkData.NodeLinkId))
        {
            LinkDictionary.Remove(linkController.LinkData.NodeLinkId);
        }
        Debug.Log(linkController.LinkData.NodeLinkId + " was removed from the link dictionary.");
    }
    public LinkController FindLinkController(string nodeLinkId)
    {
        if (LinkDictionary.TryGetValue(nodeLinkId, out LinkController linkController))
        {
            return linkController;
        }
        return null;
    }
    //public void ChangeLinkChildTarget(NodeController parentController, NodeController childController)
    //{
    //    List<LinkData> existingLinks = new List<LinkData>();

    //    LinkData[] allLinkData = FindObjectsOfType<LinkData>();
    //}
    //public List<LinkData> FindExistingLinkControllers()
    //{
    //    List<LinkData> existingLinks = new List<LinkData>();

    //    LinkData[] allLinkData = FindObjectsOfType<LinkData>();



    //    return existingLinks;
    //}
    public void UpdateRelationCountBetween(NodeController nodeA, NodeController nodeB)
    {
        int totalRelations = 0;
        int relationIndex = 0;
        List<LinkController> RelatedLinkControllers = new List<LinkController>();
        LinkController[] allNodeLinks = GameObject.FindObjectsOfType<LinkController>();


        for (int i = 0; i < allNodeLinks.Length; i++)
        {
            LinkController linkController = allNodeLinks[i];

            if ((linkController.ParentNode == nodeA && linkController.ChildNode == nodeB) || (linkController.ParentNode == nodeB && linkController.ChildNode == nodeA))
            {
                totalRelations++;
                relationIndex++;
                linkController.relationIndex = relationIndex;

                RelatedLinkControllers.Add(linkController);
            }
        }

        foreach (LinkController item in RelatedLinkControllers)
        {
            item.TotalRelatedLinks = totalRelations;
            item.SetLineLabelPoint();
        }
    }

    #endregion

    #region Question

    public QuestionData CreateNewQuestionData(NodeData nodeData)
    {
        QuestionData questionData = new QuestionData();
        questionData.NodeId = nodeData.NodeId;
        questionData.NodeType = nodeData.NodeType;
        questionData.QuestionId = IDGenerator.GenerateID();
        questionData.ChoiceId = IDGenerator.GenerateID();
        questionData.OrderId = 0;
        questionData.QuestionText = "Placeholder Question Text";
        questionData.ChoiceText = "Placeholder Question Choice Text";
        questionData.QuestionChoices = new List<ChoiceData>();
        return questionData;
    }
    public QuestionData CopyQuestionData(QuestionData copyData)
    {
        QuestionData questionData = new QuestionData();
        questionData.NodeId = copyData.NodeId;
        questionData.NodeType = copyData.NodeType;
        questionData.QuestionId = IDGenerator.GenerateID();
        questionData.ChoiceId = IDGenerator.GenerateID();
        questionData.OrderId = 0;
        questionData.QuestionText = "Placeholder Question Text";
        questionData.ChoiceText = "Placeholder Question Choice Text";
        questionData.QuestionChoices = new List<ChoiceData>();

        foreach (var item in copyData.QuestionChoices)
        {
            if (!questionData.QuestionChoices.Contains(item))
            {
                questionData.QuestionChoices.Add(CopyChoiceData(item));
            }
        }

        return questionData;
    }
    #endregion

    #region Answer
    public ChoiceData CreateNewChoiceData(QuestionData questionData)
    {
        ChoiceData data = new ChoiceData();
        data.choiceId = IDGenerator.GenerateID();
        data.questionId = questionData.QuestionId;
        data.orderId = 0;
        data.choiceText = "Placeholder Choice Text";
        data.feedback = "Placeholder Feedback Text";
        data.correct = false;
        return data;
    }
    public static ChoiceData CopyChoiceData(ChoiceData copyData)
    {
        ChoiceData data = new ChoiceData();
        data.choiceId = IDGenerator.GenerateID();
        data.questionId = copyData.questionId;
        data.orderId = copyData.orderId;
        data.choiceText = copyData.choiceText;
        data.feedback = copyData.feedback;
        data.correct = copyData.correct;
        return data;
    }
    #endregion
}
