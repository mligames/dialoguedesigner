﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public static class IDGenerator
{
    #region Data
    public static string GenerateID()
    {
        using (MD5 md5 = MD5.Create())
        {
            byte[] hash = md5.ComputeHash(
                Encoding.Default.GetBytes(String.Concat(GenerateRandomString(), System.DateTime.UtcNow.ToString()))
                ); 
            Guid result = new Guid(hash);
            result.ToString();
            return result.ToString();
        }
    }
    private static string GenerateRandomString()
    {
        int randomInt = UnityEngine.Random.Range(10000, 99999);
        return randomInt.ToString();
    }
    #endregion
}
