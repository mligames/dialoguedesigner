﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct Boundary
{
    public float xMin;
    public float yMin;
    public float xMax;
    public float yMax;

    public Boundary(float xMin, float yMin, float xMax, float yMax)
    {
        this.xMin = xMin;
        this.yMin = yMin;
        this.xMax = xMax;
        this.yMax = yMax;
    }
}

public class MapBoundary : Singleton<MapBoundary>
{
    private LineRenderer lineRenderer;

    public Boundary MaxBoundry;
    // Start is called before the first frame update
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        DrawBoundryLines();
        Debug.Log("CameraPixel Width " + Camera.main.pixelWidth + " Scaled: " + Camera.main.scaledPixelWidth);
        Debug.Log("CameraPixel Width " + Camera.main.pixelHeight + " Scaled: " + Camera.main.scaledPixelHeight);

        Camera.main.transform.position = new Vector2(MaxBoundry.xMin + 100, MaxBoundry.yMax - 100);
    }

    public void CalculateMinMaxBounds()
    {

        float xMin = MaxBoundry.xMin;
        float yMin = MaxBoundry.yMin;
        float xMax = MaxBoundry.xMax;
        float yMax = MaxBoundry.yMax;

        NodeController[] allNodeControllers = GameObject.FindObjectsOfType<NodeController>();
        // Ever Expanding
        for (int i = 0; i < allNodeControllers.Length; i++)
        {
            Vector2 temp = allNodeControllers[i].transform.position;
 
            if(temp.x < xMin)
            {
                xMin = temp.x;
            }
            if (temp.x > xMax)
            {
                xMax = temp.x;
            }
            if (temp.y < yMin)
            {
                yMin = temp.y;
            }
            if (temp.y > yMax)
            {
                yMax = temp.y;
            }
        }

        MaxBoundry = new Boundary(xMin, yMin, xMax, yMax);

        DrawBoundryLines();
    }
    public void DrawBoundryLines()
    {
        lineRenderer.SetPosition(0, new Vector2(MaxBoundry.xMax + 100, MaxBoundry.yMax + 75));
        lineRenderer.SetPosition(1, new Vector2(MaxBoundry.xMax + 100, MaxBoundry.yMin - 75));
        lineRenderer.SetPosition(2, new Vector2(MaxBoundry.xMin - 100, MaxBoundry.yMin - 75));
        lineRenderer.SetPosition(3, new Vector2(MaxBoundry.xMin - 100, MaxBoundry.yMax + 75));
    }

}
