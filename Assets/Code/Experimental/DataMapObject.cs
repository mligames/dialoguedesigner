﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DataMapObject : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public DataModel Data;

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("OnPointerEnter DataMapObject");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("OnPointerExit DataMapObject");
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
