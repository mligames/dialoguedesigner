﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DataView : MonoBehaviour
{
    public DataMapObject MapObject;
    public TMP_InputField TMP_InputField;
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.SetActive(false);
    }

    public void Init(DataMapObject mapObject)
    {
        MapObject = mapObject;
        TMP_InputField.text = mapObject.Data.NameValue;
    }
}
