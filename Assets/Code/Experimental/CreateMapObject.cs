﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using TMPro.Examples;
using UnityEngine;
using UnityEngine.UI;

public class CreateMapObject : MonoBehaviour
{
    public DataMapObject MapObjectPrefab;
    public DataView MapObjectViewPrefab;
    public Canvas ViewCanvas;

    public TMP_InputField TMP_InputField;

    public Button CreateButton;
    // Start is called before the first frame update
    void Start()
    {
        CreateButton.onClick.AddListener(Create);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void Create()
    {
       
        DataMapObject mapObject = GameObject.Instantiate(MapObjectPrefab);
        mapObject.Data = ScriptableObject.CreateInstance<DataModel>();
        mapObject.Data.NameValue = TMP_InputField.text;
        DataView viewObject = GameObject.Instantiate(MapObjectViewPrefab, ViewCanvas.transform);
        viewObject.Init(mapObject);
        //viewObject.
    }
}
