﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CounterData : ScriptableObject
{
    public string counterId;
    public string counterCaseId;
    public string counterName;
    public string counterDescription;
    public float counterInitialValue;
    public bool counterShowInPlayer;
    public int counterUnitId;
}
//Get Default Counters
// http://136.142.96.150:8090/api/DefaultCounters
//[{
//	"counterId": -1,
//	"counterName": "Steps",
//	"counterDescription": "Track how many steps it takes a learner to reach a case end-point (terminal node)",
//	"allowCustomLabel": false,
//	"counterDisplayOrder": 3,
//	"selectedCounters": []
//}, {
//	"counterId": 0,
//	"counterName": "Real Time",
//	"counterDescription": "Track how much real time a learner takes to reach a case end-point (terminal node)",
//	"allowCustomLabel": false,
//	"counterDisplayOrder": 2,
//	"selectedCounters": []
//}, {
//	"counterId": 1,
//	"counterName": "Cost",
//	"counterDescription": "Track how much money a learner spends or makes in this case",
//	"allowCustomLabel": false,
//	"counterDisplayOrder": 1,
//	"selectedCounters": []
//}, {
//	"counterId": 2,
//	"counterName": "Time",
//	"counterDescription": "Track how much “case time” (author-defined) a learner uses in this case",
//	"allowCustomLabel": false,
//	"counterDisplayOrder": 4,
//	"selectedCounters": []
//}, {
//	"counterId": 3,
//	"counterName": "Score",
//	"counterDescription": "Track how many points a learner gains or loses in this case",
//	"allowCustomLabel": true,
//	"counterDisplayOrder": 5,
//	"selectedCounters": []
//}, {
//	"counterId": 4,
//	"counterName": "Status",
//	"counterDescription": "Track patient status (e.g. clinical status, pain status, satisfaction, etc.)",
//	"allowCustomLabel": true,
//	"counterDisplayOrder": 6,
//	"selectedCounters": []
//}, {
//	"counterId": 6,
//	"counterName": "Custom 1",
//	"counterDescription": "Custom counter 1",
//	"allowCustomLabel": true,
//	"counterDisplayOrder": 7,
//	"selectedCounters": []
//}, {
//	"counterId": 7,
//	"counterName": "Custom 2",
//	"counterDescription": "Custom counter 2",
//	"allowCustomLabel": true,
//	"counterDisplayOrder": 8,
//	"selectedCounters": []
//}, {
//	"counterId": 8,
//	"counterName": "Custom 3",
//	"counterDescription": "Custom counter 3",
//	"allowCustomLabel": true,
//	"counterDisplayOrder": 9,
//	"selectedCounters": []
//}, {
//	"counterId": 9,
//	"counterName": "Custom 4",
//	"counterDescription": "Custom counter 4",
//	"allowCustomLabel": true,
//	"counterDisplayOrder": 10,
//	"selectedCounters": []
//}]


// Get selected counters for a case by caseId
// http://136.142.96.150:8090/api/SelectedCounters/CaseCounters/(CaseId)
//[{
//	"selectedCounterId": 7,
//	"counterId": -1,
//	"caseId": "8d2ae98d-e726-4fe3-abe0-4cc513e5ff93",
//	"initialValue": 0.0,
//	"showInPlayer": false,
//	"counterUnitId": -1
//}, {
//	"selectedCounterId": 8,
//	"counterId": 2,
//	"caseId": "8d2ae98d-e726-4fe3-abe0-4cc513e5ff93",
//	"initialValue": 100.0,
//	"showInPlayer": true,
//	"counterUnitId": -1
//}]