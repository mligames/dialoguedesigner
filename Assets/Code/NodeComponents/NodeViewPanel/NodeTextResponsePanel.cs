﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NodeTextResponsePanel : NodeDataPanel
{
    [SerializeField] private NodeController selectedController;
    public override NodeController SelectedController { get => selectedController; set => selectedController = value; }

    public TMP_InputField Prompt_InputField;
    public TMP_InputField Feedback_InputField;

    private void Start()
    {
        Prompt_InputField.onValueChanged.AddListener(ChangeQuestionText);
        Feedback_InputField.onValueChanged.AddListener(ChangeFeedbackText);
    }

    public override void ActivatePanel(NodeController selectedController)
    {
        this.selectedController = selectedController;
        Prompt_InputField.text = SelectedController.NodeData.QuestionData.QuestionText;
        Feedback_InputField.text = SelectedController.NodeData.QuestionData.ChoiceText;
    }

    private void ChangeQuestionText(string value)
    {
        SelectedController.NodeData.QuestionData.QuestionText = value;
    }
    private void ChangeFeedbackText(string value)
    {
        SelectedController.NodeData.QuestionData.ChoiceText = value;
    }
    public override void DeactivatePanel(NodeController selectedController)
    {
        selectedController = null;
    }
}
