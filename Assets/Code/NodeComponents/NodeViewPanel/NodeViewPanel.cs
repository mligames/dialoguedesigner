﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NodeViewPanel : MonoBehaviour
{
    [SerializeField] private NodeController selectedController;
    public NodeController SelectedController { get => selectedController; set => selectedController = value; }

    #region Panel References
    [SerializeField] private NodeTypePanel nodeTypePanel;
    public NodeTypePanel NodeTypePanel { get => nodeTypePanel; set => nodeTypePanel = value; }

    [SerializeField] private NodeTitlePanel nodeTitlePanel;
    public NodeTitlePanel NodeTitlePanel { get => nodeTitlePanel; set => nodeTitlePanel = value; }

    [SerializeField] private NodeTextPanel nodeTextPanel;
    public NodeTextPanel NodeTextPanel { get => nodeTextPanel; set => nodeTextPanel = value; }

    [SerializeField] private NodeMCQPanel nodeMCQPanel;
    public NodeMCQPanel NodeMCQPanel { get => nodeMCQPanel; set => nodeMCQPanel = value; }

    [SerializeField] private NodeInquiryPanel nodeInquiryPanel;
    public NodeInquiryPanel NodeInquiryPanel { get => nodeInquiryPanel; set => nodeInquiryPanel = value; }

    [SerializeField] private NodeTextResponsePanel nodeTextResponsePanel;
    public NodeTextResponsePanel NodeTextResponsePanel { get => nodeTextResponsePanel; set => nodeTextResponsePanel = value; }

    [SerializeField] private NodeLinksPanel nodeLinksPanel;
    public NodeLinksPanel NodeLinksPanel { get => nodeLinksPanel; set => nodeLinksPanel = value; }
    #endregion
    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void RefreshPanel()
    {
        gameObject.SetActive(false);
        gameObject.SetActive(true);
    }

    public void DisplayPanel(NodeController nodeController)
    {
        if (SelectedController != null)
        {
            SelectedController.DeselectNode();
            //SelectedController.UpdateNodeDataAPI(); // Update API Call
        }
        nodeController.SelectNode();
        // Call For Node Data.

        SelectedController = nodeController;
        //SelectedNode = nodeController.NodeData;

        //InspectorPanel.gameObject.SetActive(true);

        //IDPanel.gameObject.SetActive(true);
        //IDPanel.ActivatePanel(SelectedNode);

        nodeTypePanel.gameObject.SetActive(true);
        nodeTypePanel.ActivatePanel(SelectedController);

        nodeTitlePanel.gameObject.SetActive(true);
        nodeTitlePanel.ActivatePanel(SelectedController);

        nodeTextPanel.gameObject.SetActive(true);
        nodeTextPanel.ActivatePanel(SelectedController);


        // Add Break for debuging here
        switch (SelectedController.NodeData.NodeType)
        {
            case NodeType.NARRATIVE:
                break;
            case NodeType.MCQ:
                nodeMCQPanel.gameObject.SetActive(true);
                nodeMCQPanel.ActivatePanel(SelectedController);
                break;
            case NodeType.BRANCHING:
                break;
            case NodeType.RANDOM_ROUTE:
                break;
            case NodeType.LOGIC:
                break;
            case NodeType.INQUIRY:
                nodeInquiryPanel.gameObject.SetActive(true);
                nodeInquiryPanel.ActivatePanel(SelectedController);
                break;
            case NodeType.TEXT_RESPONSE:
                nodeTextResponsePanel.gameObject.SetActive(true);
                nodeTextResponsePanel.ActivatePanel(SelectedController);
                break;
            case NodeType.UNDEFINED:
                break;
        }

        if (SelectedController.NodeData.NodeLinks.Count > 0)
        {
            nodeLinksPanel.gameObject.SetActive(true);
            nodeLinksPanel.ActivatePanel(SelectedController);
        }

        //InspectorPanel.gameObject.SetActive(true);
    }
    //public void ActivateDataPanels()
    //{
    //    nodeTypePanel.ActivatePanel();
    //    nodeTitlePanel.ActivatePanel();
    //    nodeTextPanel.ActivatePanel();

    //    switch (selectedController.NodeData.NodeType)
    //    {
    //        case NodeType.NARRATIVE:
    //            nodeMCQPanel.DeactivatePanel();
    //            nodeInquiryPanel.DeactivatePanel();
    //            nodeTextResponsePanel.DeactivatePanel();
    //            break;
    //        case NodeType.MCQ:

    //            MCQNodeData mcqData = (MCQNodeData)selectedController.NodeData;
    //            nodeMCQPanel.ActivatePanel(mcqData.Question);

    //            nodeInquiryPanel.DeactivatePanel();
    //            nodeTextResponsePanel.DeactivatePanel();
    //            break;
    //        case NodeType.BRANCHING:
    //            nodeMCQPanel.DeactivatePanel();
    //            nodeInquiryPanel.DeactivatePanel();
    //            nodeTextResponsePanel.DeactivatePanel();
    //            break;
    //        case NodeType.RANDOM_ROUTE:
    //            break;
    //        case NodeType.LOGIC:
    //            break;
    //        case NodeType.INQUIRY:
    //            nodeMCQPanel.DeactivatePanel();
    //            nodeInquiryPanel.ActivatePanel();
    //            nodeTextResponsePanel.DeactivatePanel();
    //            break;
    //        case NodeType.TEXT_RESPONSE:
    //            nodeMCQPanel.DeactivatePanel();
    //            nodeInquiryPanel.DeactivatePanel();
    //            nodeTextResponsePanel.ActivatePanel();
    //            break;
    //        case NodeType.UNDEFINED:
    //            break;
    //    }
    //    if (selectedController.NodeData.NodeLinks.Count > 0)
    //    {
    //        nodeLinksPanel.ActivatePanel();
    //    }
    //    // Get Type of NodeData
    //    // Activate Deactivate Panels Activate 
    //}

    public void DisplayPanel()
    {
        gameObject.SetActive(true);
    }
    public void HidePanel()
    {
        gameObject.SetActive(false);
    }
}
