﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NodeTitlePanel : NodeDataPanel
{
    [SerializeField] private NodeController selectedController;
    public override NodeController SelectedController { get => selectedController; set => selectedController = value; }


    [SerializeField] private TMP_InputField nodeTitle_InputField;
    public TMP_InputField NodeTitle_InputField { get => nodeTitle_InputField; set => nodeTitle_InputField = value; }


    private void Start()
    {
        nodeTitle_InputField.onValueChanged.AddListener(ChangeTitle);
    }
    public override void ActivatePanel(NodeController selectedController)
    {
        SelectedController = selectedController;
        nodeTitle_InputField.text = SelectedController.NodeData.NodeTitle;
    }

    private void ChangeTitle(string value)
    {
        SelectedController.NodeData.NodeTitle = value;
    }

    public override void DeactivatePanel(NodeController selectedController)
    {
        SelectedController = null;
    }
}
