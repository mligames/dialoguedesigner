﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class NodeOptionData : TMP_Dropdown.OptionData
{
    public NodeController NodeController { get; set; }
}
