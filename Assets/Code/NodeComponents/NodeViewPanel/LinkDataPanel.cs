﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LinkDataPanel : MonoBehaviour
{
    //#region Data
    //[SerializeField] private LinkData linkData;
    //public LinkData LinkData { get => linkData; set => linkData = value; }
    //#endregion

    #region Components
    [Header("Components")]
    [SerializeField] private NodeLinksPanel nodeLinksPanel;
    public NodeLinksPanel NodeLinksPanel { get => nodeLinksPanel; set => nodeLinksPanel = value; }

    [SerializeField] private LinkController linkController;
    public LinkController LinkController { get => linkController; set => linkController = value; }

    [SerializeField] private TMP_InputField linkText_InputField;
    public TMP_InputField LinkText_InputField { get => linkText_InputField; set => linkText_InputField = value; }

    [SerializeField] private TMP_Text linkTargetInfo_Text;
    public TMP_Text LinkTargetInfo_Text { get => linkTargetInfo_Text; set => linkTargetInfo_Text = value; }

    [SerializeField] private TMP_Dropdown retarget_Dropdown;
    public TMP_Dropdown Retarget_Dropdown { get => retarget_Dropdown; set => retarget_Dropdown = value; }

    [SerializeField] private Button moveUp_Button;
    public Button MoveUp_Button { get => moveUp_Button; set => moveUp_Button = value; }

    [SerializeField] private Button moveDown_Button;
    public Button MoveDown_Button { get => moveDown_Button; set => moveDown_Button = value; }

    [SerializeField] private Button remove_Button;
    public Button Remove_Button { get => remove_Button; set => remove_Button = value; }

    #endregion

    #region Variables
    [SerializeField] private bool isPrefab;
    public bool IsPrefab { get => isPrefab; set => isPrefab = value; }

    public List<NodeController> AllNodesList;
    //List<NodeController> AllNodesList;
    #endregion

    private void Awake()
    {
        NodeLinksPanel = GetComponentInParent<NodeLinksPanel>();
    }
    private void Start()
    {
        retarget_Dropdown.onValueChanged.AddListener(OnDropdownSelect);
        linkText_InputField.onValueChanged.AddListener(SetLinkDataLinkText);

        //MoveUp_Button.onClick.AddListener(MoveUp);
        //MoveDown_Button.onClick.AddListener(MoveDown);

        remove_Button.onClick.AddListener(DeleteLink);
        //LinkLabelField.onValueChanged.AddListener(SetNodeLinkLabel);
    }

    public void SetupComponent(LinkData linkData)
    {
        LinkController = linkData.AssignedLinkController;
        LinkText_InputField.text = linkData.LinkText;

        SetNodeLinkLabel(linkData.LinkText);

        AllNodesList = new List<NodeController>();
        NodeController[] allNodes = FindObjectsOfType<NodeController>();

        for (int i = 0; i < allNodes.Length; i++)
        {
            //Debug.Log(NodeLinksPanel.SelectedController.name);
            if (!AllNodesList.Contains(allNodes[i]) && allNodes[i] != NodeLinksPanel.SelectedController)
            {
                AllNodesList.Add(allNodes[i]);
            }
        }

        for (int i = 0; i < AllNodesList.Count; i++)
        {
            NodeOptionData optionData = new NodeOptionData
            {
                text = AllNodesList[i].NodeData.NodeTitle,
                NodeController = AllNodesList[i]
            };
            Retarget_Dropdown.options.Add(optionData);
        }

        for (int i = 0; i < Retarget_Dropdown.options.Count; i++)
        {
            NodeOptionData optionData = (NodeOptionData)Retarget_Dropdown.options[i];
            if (optionData.NodeController == LinkController.ChildNode)
            {
                Retarget_Dropdown.value = i;
                break;
            }
        }

        Retarget_Dropdown.RefreshShownValue();
    }
    public void SetNodeLinkLabel(string value)
    {
        linkController.LinkData.LinkText = value;
        linkController.LinkLabel_InputField.text = value;
    }

    private void OnDropdownSelect(int value)
    {
        NodeOptionData optionData = (NodeOptionData)retarget_Dropdown.options[value];
        linkController.AssignChild(optionData.NodeController);
    }
    private void SetLinkDataLinkText(string value)
    {
        linkText_InputField.text = value;
        linkController.LinkData.LinkText = value;
        if(linkController.LinkLabel_InputField != null)
        {
            linkController.LinkLabel_InputField.text = value;
        }
    }
    private void SetLinkTargetInfo(string value)
    {
        linkTargetInfo_Text.text = value;
    }
    private void DeleteLink()
    {
        nodeLinksPanel.DeleteLink(this);
    }
}
