﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeLinksPanel : NodeDataPanel
{
    [SerializeField] private NodeController selectedController;
    public override NodeController SelectedController { get => selectedController; set => selectedController = value; }

    //[SerializeField] private NodeData nodeData;
    //public override NodeData NodeData { get => nodeData; set => nodeData = value; }

    public LinkDataPanel LinkDataPanelPrefab;

    public List<LinkDataPanel> LinkDataPanelList;

    public int minHeirarchy;

    private void Awake()
    {
        LinkDataPanelList = new List<LinkDataPanel>();
    }
    private void Start()
    {
        LinkDataPanelPrefab.IsPrefab = true;
        minHeirarchy = LinkDataPanelPrefab.transform.GetSiblingIndex();
        LinkDataPanelPrefab.gameObject.SetActive(false);
        LinkDataPanelList = new List<LinkDataPanel>();
    }
    public override void ActivatePanel(NodeController nodeController)
    {
        ClearLinkPanels();
        selectedController = nodeController;

        if (selectedController.NodeData.NodeLinks.Count > 0)
        {
            for (int i = 0; i < selectedController.NodeData.NodeLinks.Count; i++)
            {
                LinkDataPanel linkPanel = Instantiate<LinkDataPanel>(LinkDataPanelPrefab, transform);
                linkPanel.gameObject.SetActive(true);
                linkPanel.gameObject.name = "LinkDataPanel";
                linkPanel.IsPrefab = false;

                linkPanel.SetupComponent(nodeController.ChildNodeLinks[i].LinkData);
                LinkDataPanelList.Add(linkPanel);
            }
        }
    }

    public void RefreshPanel()
    {

    }

    public override void DeactivatePanel(NodeController selectedController)
    {
        SelectedController = null;
        ClearLinkPanels();
    }
    public void DeleteLink(LinkDataPanel linkPanel)
    {
        linkPanel.LinkController.DeleteLink();
        Destroy(linkPanel.gameObject);
        NodeViewManager.Instance.RefreshInspectorPanel();
    }
    public void ChangeNodeLinkTarget(LinkDataPanel linkPanel, LinkController linkController, NodeController targetController)
    {
        Debug.Log("TODO: IMPLEMENT ChangeNodeLink");

        linkController.AssignChild(targetController);

        Debug.Log("NodeLinkPanels -> ChangeNodeLinkTarget Updates LinkRelations");
        DesignerManager.Instance.UpdateRelationCountBetween(linkController.ParentNode, linkController.ChildNode);
    }

    public void ClearLinkPanels()
    {
        LinkDataPanel[] linkPanels = GetComponentsInChildren<LinkDataPanel>();

        for (int i = 0; i < linkPanels.Length; i++)
        {
            if (!linkPanels[i].IsPrefab)
            {
                if (LinkDataPanelList.Contains(linkPanels[i]))
                {
                    LinkDataPanelList.Remove(linkPanels[i]);
                }
                Destroy(linkPanels[i].gameObject);
            }
        }
    }
}
