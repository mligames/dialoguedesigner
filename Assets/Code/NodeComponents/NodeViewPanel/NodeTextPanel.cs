﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NodeTextPanel : NodeDataPanel
{
    [SerializeField] private NodeController selectedController;
    public override NodeController SelectedController { get => selectedController; set => selectedController = value; }

    public TMP_InputField NodeText_InputField;
    private void Start()
    {
        NodeText_InputField.isRichTextEditingAllowed = true;
        NodeText_InputField.onValueChanged.AddListener(ChangeText);
    }
    public override void ActivatePanel(NodeController selectedController)
    {
        this.selectedController = selectedController;

        NodeText_InputField.text = SelectedController.NodeData.NodeText;
    }

    private void ChangeText(string value)
    {
        SelectedController.NodeData.NodeText = value;
    }

    public override void DeactivatePanel(NodeController selectedController)
    {
        this.selectedController = null;
    }
}
