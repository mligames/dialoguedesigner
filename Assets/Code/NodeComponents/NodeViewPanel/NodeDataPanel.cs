﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class NodeDataPanel : MonoBehaviour
{
    public abstract NodeController SelectedController { get; set; }
    public abstract void ActivatePanel(NodeController selectedController);
    public abstract void DeactivatePanel(NodeController selectedController);
}