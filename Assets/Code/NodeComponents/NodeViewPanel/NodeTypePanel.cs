﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NodeTypePanel : NodeDataPanel
{
    [SerializeField] private NodeController selectedController;
    public override NodeController SelectedController { get => selectedController; set => selectedController = value; }

    public TMP_Dropdown Dropdown;

    public override void ActivatePanel(NodeController selectedController)
    {
        this.selectedController = selectedController;

        switch (SelectedController.NodeData.NodeType)
        {
            case NodeType.NARRATIVE:
                Dropdown.value = 0;
                break;
            case NodeType.MCQ:
                Dropdown.value = 1;
                break;
            case NodeType.BRANCHING:
                Dropdown.value = 2;
                break;
            case NodeType.INQUIRY:
                Dropdown.value = 3;
                break;
            case NodeType.TEXT_RESPONSE:
                Dropdown.value = 4;
                break;
        }
        Dropdown.RefreshShownValue();
    }
    public override void DeactivatePanel(NodeController selectedController)
    {
        SelectedController = null;
    }
}
