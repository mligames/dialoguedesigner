﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuestionChoicePanel : MonoBehaviour
{
    #region Components
    [Header("Components")]
    public ChoiceData ChoiceData;
    public Toggle IsCorrect_Toggle;
    public TMP_InputField ChoiceText_InputField;
    public TMP_InputField FeedbackText_InputField;
    public Button MoveUpButton;
    public Button MoveDownButton;
    public Button RemoveButton;
    #endregion
    //public OnChoiceCorrectChange onChoiceCorrectChange;
    //public OnChoiceTextChange onChoiceTextChange;
    //public OnFeedbackTextChange onFeedbackTextChange;

    public OnChoiceMoveUp onChoiceMoveUp;
    public OnChoiceMoveDown onChoiceMoveDown;
    public OnChoiceRemove onChoiceRemove;

    #region Variables
    [SerializeField] private bool isPrefab;
    public bool IsPrefab { get => isPrefab; set => isPrefab = value; }
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        ChoiceText_InputField.onValueChanged.AddListener(ChangeChoiceText);
        FeedbackText_InputField.onValueChanged.AddListener(ChangeFeedbackText);

        MoveUpButton.onClick.AddListener(MoveAnswerUp);
        MoveDownButton.onClick.AddListener(MoveAnswerDown);
        RemoveButton.onClick.AddListener(RemoveAnswer);
    }

    public void InitChoicePanel(ChoiceData choiceData)
    {
        ChoiceData = choiceData;
        ChoiceText_InputField.text = ChoiceData.choiceText;
        FeedbackText_InputField.text = ChoiceData.feedback;
    }

    private void OnDestroy()
    {
        ChoiceText_InputField.onValueChanged.RemoveListener(ChangeChoiceText);
        FeedbackText_InputField.onValueChanged.RemoveListener(ChangeFeedbackText);
    }

    public void SetUpToggleOption()
    {
        IsCorrect_Toggle.gameObject.SetActive(true);
        IsCorrect_Toggle.onValueChanged.AddListener(ChangeCorrectChoiceToggle);
    }

    private void ChangeCorrectChoiceToggle(bool value)
    {
        IsCorrect_Toggle.isOn = value;
    }

    private void ChangeChoiceText(string value)
    {
        ChoiceData.choiceText = value;
    }

    private void ChangeFeedbackText(string value)
    {
        ChoiceData.feedback = value;
    }

    private void MoveAnswerUp()
    {
        onChoiceMoveUp.Invoke(this);
    }

    private void MoveAnswerDown()
    {
        onChoiceMoveDown.Invoke(this);
    }

    private void RemoveAnswer()
    {
        onChoiceRemove.Invoke(this);
    }
}
