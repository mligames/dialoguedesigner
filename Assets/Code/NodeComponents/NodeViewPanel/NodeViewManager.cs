﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class NodeViewManager : Singleton<NodeViewManager>
{
    public NodeController SelectedController;
    //public NodeData SelectedNode;
    public GameObject NodeViewPanel;
    public Transform ContentTransform;

    //public NodeIDPanel IDPanel;
    public NodeTypePanel TypePanel;
    public NodeTitlePanel TitlePanel;
    public NodeTextPanel TextPanel;
    public NodeMCQPanel MCQPanel;
    public NodeInquiryPanel InquiryPanel;
    public NodeTextResponsePanel TextResponsePanel;
    public NodeLinksPanel LinksPanel;

    [Header("Events")]
    public OnOverUI onOverInpector;

    private void Start()
    {
        TypePanel.Dropdown.onValueChanged.AddListener(ChangeNodeType);
        TitlePanel.NodeTitle_InputField.onValueChanged.AddListener(ChangeNodeTitle);
        TextPanel.NodeText_InputField.onValueChanged.AddListener(ChangeNodeText);
        MCQPanel.QuestionText_InputField.onValueChanged.AddListener(ChangeQuestionText);

        ClosePanel();
    }

    public void RefreshInspectorPanel()
    {
        NodeViewPanel.SetActive(false);
        NodeViewPanel.SetActive(true);
    }
    public void DisplayPanel(NodeController nodeController)
    {
        if (SelectedController != null)
        {
            SelectedController.DeselectNode();
            //SelectedController.UpdateNodeDataAPI(); // Update API Call
            DeactivateAllPanels();
        }

        nodeController.SelectNode();

        SelectedController = nodeController;

        NodeViewPanel.gameObject.SetActive(true);

        TypePanel.gameObject.SetActive(true);
        TypePanel.ActivatePanel(SelectedController);

        TitlePanel.gameObject.SetActive(true);
        TitlePanel.ActivatePanel(SelectedController);

        TextPanel.gameObject.SetActive(true);
        TextPanel.ActivatePanel(SelectedController);


        // Add Break for debuging here
        switch (SelectedController.NodeData.NodeType)
        {
            case NodeType.NARRATIVE:
                break;
            case NodeType.MCQ:
                MCQPanel.gameObject.SetActive(true);
                MCQPanel.ActivatePanel(SelectedController);
                break;
            case NodeType.BRANCHING:
                break;
            case NodeType.RANDOM_ROUTE:
                break;
            case NodeType.LOGIC:
                break;
            case NodeType.INQUIRY:
                InquiryPanel.gameObject.SetActive(true);
                InquiryPanel.ActivatePanel(SelectedController);
                break;
            case NodeType.TEXT_RESPONSE:
                TextResponsePanel.gameObject.SetActive(true);
                TextResponsePanel.ActivatePanel(SelectedController);
                break;
            case NodeType.UNDEFINED:
                break;
        }

        if (SelectedController.NodeData.NodeLinks.Count > 0)
        {
            LinksPanel.gameObject.SetActive(true);
            LinksPanel.ActivatePanel(SelectedController);
        }

        RefreshInspectorPanel();
    }

    public void DeactivateAllPanels()
    {
        TypePanel.gameObject.SetActive(false);
        TitlePanel.gameObject.SetActive(false);
        TextPanel.gameObject.SetActive(false);

        if (MCQPanel.gameObject.activeSelf == true)
        {
            MCQPanel.DeactivatePanel(SelectedController);
            MCQPanel.gameObject.SetActive(false);
        }

        if (TextResponsePanel.gameObject.activeSelf == true)
        {
            TextResponsePanel.DeactivatePanel(SelectedController);
            TextResponsePanel.gameObject.SetActive(false);
        }

        if (InquiryPanel.gameObject.activeSelf == true)
        {
            InquiryPanel.DeactivatePanel(SelectedController);
            InquiryPanel.gameObject.SetActive(false);
        }

        if (LinksPanel.gameObject.activeSelf == true)
        {
            LinksPanel.DeactivatePanel(SelectedController);
            LinksPanel.gameObject.SetActive(false);
        }
    }

    public void ClosePanel()
    {
        DeactivateAllPanels();
        //UserInput.Instance.isOverUI = false;
        if (SelectedController != null)
        {
            SelectedController.DeselectNode();
            //SelectedController.UpdateNodeDataAPI(); // Update API Call
        }
        onOverInpector.Invoke(false);
        NodeViewPanel.gameObject.SetActive(false);
    }

    private NodeType ConvertDropdownValueToNodeType(int value)
    {
        switch (value)
        {
            case 0:
                return NodeType.NARRATIVE;
            case 1:
                return NodeType.MCQ;
            case 2:
                return NodeType.BRANCHING;
            case 3:
                return NodeType.INQUIRY;
            case 4:
                return NodeType.TEXT_RESPONSE;
            default:
                return SelectedController.NodeType;
        }
    }
    private void ChangeNodeType(int value)
    {
        NodeData previousData = SelectedController.NodeData;
        NodeType previousType = SelectedController.NodeData.NodeType;

        NodeType newNodeType = ConvertDropdownValueToNodeType(value);

        Debug.Log("TODO: CHECK FOR LINK COUNT AND RESTRICT MULTI LINK CONNECTIONS");

        Debug.Log(SelectedController.name + "Is Selected with type " + previousType.ToString());
        Debug.Log(SelectedController.name + " New Type is " + newNodeType.ToString());

        if (SelectedController.NodeData.NodeType == newNodeType)
        {
            return;
        }

        SelectedController.NodeData.NodeType = newNodeType;

        NodeController current = SelectedController;
        //DeactivateAllPanels();
        ClosePanel();

        DisplayPanel(current);
        RefreshInspectorPanel();
    }

    private void ChangeNodeTitle(string value)
    {

        SelectedController.ChangeNodeTitle(value);
    }

    private void ChangeNodeText(string value)
    {
        SelectedController.NodeData.NodeText = value;
    }

    private void ChangeQuestionText(string value)
    {
        SelectedController.NodeData.QuestionData.QuestionText = value;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        onOverInpector.Invoke(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        onOverInpector.Invoke(false);
    }
}
