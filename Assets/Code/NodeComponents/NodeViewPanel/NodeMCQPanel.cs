﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NodeMCQPanel : NodeDataPanel
{
    #region Data
    [SerializeField] private NodeController selectedController;
    public override NodeController SelectedController { get => selectedController; set => selectedController = value; }

    #endregion

    #region Prefabs
    [Header("Prefabs")]
    [SerializeField] private QuestionChoicePanel choicePanelPrefab;
    public QuestionChoicePanel ChoicePanelPrefab { get => choicePanelPrefab; set => choicePanelPrefab = value; }
    #endregion

    #region Components
    [SerializeField] private Transform choiceContentTransform;
    public Transform ChoiceContentTransform { get => choiceContentTransform; set => choiceContentTransform = value; }

    [SerializeField] private TMP_InputField questionText_InputField;
    public TMP_InputField QuestionText_InputField { get => questionText_InputField; set => questionText_InputField = value; }
    #endregion

    #region Variables
    [SerializeField] private string choicePanelInstanceName;
    public string ChoicePanelInstanceName { get => choicePanelInstanceName; set => choicePanelInstanceName = value; }

    public List<QuestionChoicePanel> ChoicePanelList;

    #endregion
    private void Awake()
    {
        ChoicePanelList = new List<QuestionChoicePanel>();
    }

    private void Start()
    {
        QuestionText_InputField.onValueChanged.AddListener(ChangeQuestionText);
        choicePanelPrefab.IsPrefab = true;
        ChoicePanelPrefab.gameObject.SetActive(false);
    }

    public override void ActivatePanel(NodeController selectedController)
    {
        RemoveAllChoicePanels();
        try
        {
            this.selectedController = selectedController;
            QuestionText_InputField.text = SelectedController.NodeData.QuestionData.QuestionText;
            LoadChoiceData();
        }
        catch (System.InvalidCastException)
        {
            Debug.Log(this.name + " InvalidCastException");
        }
    }

    public void LoadChoiceData()
    {
        if (SelectedController.NodeData.QuestionData.QuestionChoices.Count > 0)
        {
            for (int i = 0; i < SelectedController.NodeData.QuestionData.QuestionChoices.Count; i++)
            {
                QuestionChoicePanel choicePanel = Instantiate<QuestionChoicePanel>(ChoicePanelPrefab, ChoiceContentTransform);

                choicePanel.gameObject.SetActive(true);
                choicePanel.gameObject.name = choicePanelInstanceName;
                choicePanel.IsPrefab = false;
                choicePanel.InitChoicePanel(SelectedController.NodeData.QuestionData.QuestionChoices[i]);

                ChoicePanelList.Add(choicePanel);

                choicePanel.onChoiceMoveUp.AddListener(MoveUpHierarchy);
                choicePanel.onChoiceMoveDown.AddListener(MoveDownHierarchy);
                choicePanel.onChoiceRemove.AddListener(RemoveChoicePanel);
            }
        }
    }

    public override void DeactivatePanel(NodeController previousController)
    {
        RemoveAllChoicePanels();
        this.selectedController = null;
    }

    //public void SaveAnswerData()
    //{
    //    if (gameObject.activeSelf && nodeData != null)
    //    {
    //        //this.nodeData.Question.questionChoices.Clear();

    //        if (ChoicePropertyPanelsList.Count > 0)
    //        {
    //            this.nodeData.Question.QuestionChoices.Clear();
    //            for (int i = 0; i < ChoicePropertyPanelsList.Count; i++)
    //            {

    //                ChoiceData choice = new Choice(ChoicePropertyPanelsList[i].ChoiceData.choiceId, nodeData.Question.questionId, i, ChoicePropertyPanelsList[i].ChoiceData.choiceText, ChoicePropertyPanelsList[i].ChoiceData.feedback, ChoicePropertyPanelsList[i].ChoiceData.correct);

    //                nodeData.Question.QuestionChoices.Add(choice);
    //                Debug.Log("Saving: " + JsonUtility.ToJson(this.nodeData.Question));
    //            }
    //        }
    //    }
    //}

    public void AddNewChoice()
    {
        QuestionChoicePanel choicePanel = Instantiate<QuestionChoicePanel>(ChoicePanelPrefab, ChoiceContentTransform);
        choicePanel.gameObject.SetActive(true);
        choicePanel.gameObject.name = "MCQChoicePanel";
        ChoiceData choice = new ChoiceData(SelectedController.NodeData.QuestionData);

        SelectedController.NodeData.QuestionData.QuestionChoices.Add(choice);

        choicePanel.ChoiceData = choice;

        ChoicePanelList.Add(choicePanel);

        choicePanel.onChoiceMoveUp.AddListener(MoveUpHierarchy);
        choicePanel.onChoiceMoveDown.AddListener(MoveDownHierarchy);
        choicePanel.onChoiceRemove.AddListener(RemoveChoicePanel);

        NodeViewManager.Instance.RefreshInspectorPanel();
    }
    public void RemoveChoice(ChoiceData choice)
    {
        if (SelectedController.NodeData.QuestionData.QuestionChoices.Contains(choice))
        {
            SelectedController.NodeData.QuestionData.QuestionChoices.Remove(choice);
        }
    }
    public void RemoveAllChoicePanels()
    {
        if (ChoicePanelList.Count > 0)
        {
            for (int i = ChoicePanelList.Count - 1; i >= 0; i--)
            {
                QuestionChoicePanel choicePanel = ChoicePanelList[i];

                if (choicePanel.IsPrefab)
                {
                    continue;
                }

                if (ChoicePanelList.Contains(choicePanel))
                {
                    ChoicePanelList.Remove(choicePanel);
                }

                choicePanel.onChoiceMoveUp.RemoveListener(MoveUpHierarchy);
                choicePanel.onChoiceMoveDown.RemoveListener(MoveDownHierarchy);
                choicePanel.onChoiceRemove.RemoveListener(RemoveChoicePanel);
                Destroy(choicePanel.gameObject);
            }
        }
    }

    public void RemoveChoicePanel(QuestionChoicePanel choicePanel)
    {
        RemoveChoice(choicePanel.ChoiceData);

        if (ChoicePanelList.Contains(choicePanel))
        {
            ChoicePanelList.Remove(choicePanel);
        }

        choicePanel.onChoiceMoveUp.RemoveListener(MoveUpHierarchy);
        choicePanel.onChoiceMoveDown.RemoveListener(MoveDownHierarchy);
        choicePanel.onChoiceRemove.RemoveListener(RemoveChoicePanel);

        Destroy(choicePanel.gameObject);
        NodeViewManager.Instance.RefreshInspectorPanel();
    }

    public void ChangeQuestionText(string value)
    {
        SelectedController.NodeData.QuestionData.QuestionText = value;
    }

    public void MoveUpHierarchy(QuestionChoicePanel choicePanel)
    {
        int siblingIndex = choicePanel.transform.GetSiblingIndex();
        if (siblingIndex != 0)
        {
            choicePanel.transform.SetSiblingIndex(siblingIndex - 1);
        }
    }

    public void MoveDownHierarchy(QuestionChoicePanel choicePanel)
    {
        int siblingIndex = choicePanel.transform.GetSiblingIndex();
        choicePanel.transform.SetSiblingIndex(siblingIndex + 1);
    }
}
