﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Collider2D))]
public class ClickDragMovement : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Transform DragTransform;
    public Vector2 CurrentPosition { get; set; }
    public Collider2D CollierComp { get; set; }

    public Vector2 offest;
    public OnBeginDrag onNodeDragBegin;
    public OnEndDrag onNodeDragEnd;

    public OnPositionChanged onPositionChanged;
    // Start is called before the first frame update
    void Start()
    {
        if(DragTransform == null)
        {
            DragTransform = transform;
        }
        CollierComp = GetComponent<Collider2D>();
        CurrentPosition = DragTransform.position;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        offest = DragTransform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
        onNodeDragBegin.Invoke();
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        Vector2 position = mousePos + offest;
        DragTransform.position = position;
        onPositionChanged.Invoke(position);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //TODO: Send Drag Done State
        onNodeDragEnd.Invoke();
    }

    private void OnDestroy()
    {
        onPositionChanged.RemoveAllListeners();
    }
}
