﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public enum NodeControllerState { IDLE, MOUSE_OVER, DRAGGING_NODE }

public class NodeController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [Header("State")]
    [SerializeField] private NodeControllerState currentState;
    public NodeControllerState CurrentState { get => currentState; set => currentState = value; }

    [Header("Data")]
    [SerializeField] NodeData nodeData;
    public NodeData NodeData { get => nodeData; set => nodeData = value; }
    public string NodeId { get => NodeData.NodeId; }
    public NodeType NodeType { get => NodeData.NodeType; }
    public string NodeTitle { get => NodeData.NodeTitle; }

    [Header("Components")]
    [SerializeField] private TMP_Text nodeTitle_Text;
    public TMP_Text NodeTitle_Text { get => nodeTitle_Text; set => nodeTitle_Text = value; }

    [SerializeField] private SpriteRenderer nodeSprite;
    public SpriteRenderer NodeSprite { get => nodeSprite; set => nodeSprite = value; }

    [SerializeField] private SpriteRenderer boarderSprite;
    public SpriteRenderer BoarderSprite { get => boarderSprite; set => boarderSprite = value; }


    public ColorSwatch defaultColorSwatch;
    public ColorSwatch highlightColorSwatch;

    public List<LinkController> ParentNodeLinks;
    public List<LinkController> ChildNodeLinks;

    public ClickDragMovement DragMovement;
    public MapObjectClickHandler ClickHandler;
    public CreateLinkOnDrag CreateLinkHandler;

    public OnDeleteNode onDelete;

    private void Awake()
    {
        //RelatedLinkControllerList = new List<LinkController>();
        ParentNodeLinks = new List<LinkController>();
        ChildNodeLinks = new List<LinkController>();
    }
    private void Start()
    {
        if (DragMovement)
        {
            DragMovement.onNodeDragBegin.AddListener(OnNodeDragBegin);
            DragMovement.onNodeDragEnd.AddListener(OnNodeDragEnd);
            DragMovement.onPositionChanged.AddListener(OnNodePositionChange);
        }
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        UserCamera.Instance.IsPanningBlocked = true;
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        UserCamera.Instance.IsPanningBlocked = false;
    }
    public void DisplayNodeInspector()
    {
        NodeViewManager.Instance.DisplayPanel(this);
    }
    public void AddRelatedLink(LinkController linkController)
    {
        linkController.onDelete.AddListener(RemoveRelatedLink);
        if (linkController.LinkData.ParentController == this)
        {
            AddLinkToChildList(linkController);
        }
        if (linkController.LinkData.ChildController == this)
        {
            AddLinkToParentList(linkController);
        }

        DesignerManager.Instance.UpdateRelationCountBetween(linkController.LinkData.ParentController, linkController.LinkData.ChildController);

    }
    public void RemoveRelatedLink(LinkController linkController)
    {
        if (linkController.LinkData.ParentController == this)
        {
            RemoveChildLink(linkController);
        }
        if (linkController.LinkData.ChildController == this)
        {
            RemoveParentLink(linkController);
        }

        foreach (LinkController nodeLink in ParentNodeLinks)
        {
            DesignerManager.Instance.UpdateRelationCountBetween(nodeLink.ParentNode, nodeLink.ChildNode);
        }

        foreach (LinkController nodeLink in ChildNodeLinks)
        {
            DesignerManager.Instance.UpdateRelationCountBetween(nodeLink.ParentNode, nodeLink.ChildNode);
        }
    }
    private void AddLinkToParentList(LinkController linkController)
    {
        if (!ParentNodeLinks.Contains(linkController))
        {
            ParentNodeLinks.Add(linkController);
        }
    }
    private void RemoveParentLink(LinkController linkController)
    {
        if (ParentNodeLinks.Contains(linkController))
        {
            ParentNodeLinks.Remove(linkController);
        }
    }
    private void AddLinkToChildList(LinkController linkController)
    {
        if (!ChildNodeLinks.Contains(linkController))
        {
            ChildNodeLinks.Add(linkController);
        }
        if (!NodeData.NodeLinks.Contains(linkController.LinkData))
        {
            NodeData.NodeLinks.Add(linkController.LinkData);
        }
    }
    private void RemoveChildLink(LinkController linkController)
    {
        if (ChildNodeLinks.Contains(linkController))
        {
            ChildNodeLinks.Remove(linkController);
        }

        if (NodeData.NodeLinks.Contains(linkController.LinkData))
        {

            NodeData.NodeLinks.Remove(linkController.LinkData);
        }
    }

    private void RefreshViewPanel()
    {
        if(NodeViewManager.Instance.SelectedController == this)
        {
            NodeViewManager.Instance.DisplayPanel(this);
        }

    }

    private void OnNodePositionChange(Vector2 pos)
    {
        //UserController.Instance.SelectedNodeController = this;
        //UpdateLinkLabelIndexes();
    }
    private void OnNodeDragBegin()
    {
        //TODO: Send NODE DRAGGING STATE
        CurrentState = NodeControllerState.DRAGGING_NODE;
    }
    private void OnNodeDragEnd()
    {
        //TODO: Send NODE DRAGGING END STATE
        CurrentState = NodeControllerState.IDLE;
        //NodeData.NodeCoordinates = 
    }

    public void ChangeNodeTitle(string value)
    {
        NodeData.NodeTitle = value;
        nodeTitle_Text.text = value;       
    }

    public void SelectNode()
    {
        string colorValue = "#" + highlightColorSwatch.hex;
        if (ColorUtility.TryParseHtmlString(colorValue, out Color color))
        {
            boarderSprite.color = color;
        }
    }
    public void DeselectNode()
    {
        string colorValue = "#" + defaultColorSwatch.hex;
        if (ColorUtility.TryParseHtmlString(colorValue, out Color color))
        {
            boarderSprite.color = color;
        }
    }
    public void DeleteNode()
    {
        if (NodeViewManager.Instance.SelectedController == this)
        {
            NodeViewManager.Instance.ClosePanel();
        }

        Debug.Log("Deleting Node Controller...");
        onDelete.Invoke();
        //string url = APIController.Instance.NodeDataUrl(NodeData.NodeId);
        //StartCoroutine(APIController.Instance.HttpDelete(url, (r) => OnDeleteRequestNodeData(r)));
        StartCoroutine(SafeDeleteRoutine());
    }
    IEnumerator SafeDeleteRoutine()
    {
        yield return null; // Yield for 1 frame.
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        onDelete.RemoveAllListeners();
    }


}
