﻿using System;
using UnityEngine.Events;

[Serializable]
public class OnDeleteNode : UnityEvent { }

