﻿using System;
using UnityEngine.Events;

[Serializable]
public class OnDeleteLink : UnityEvent<LinkController> { }
