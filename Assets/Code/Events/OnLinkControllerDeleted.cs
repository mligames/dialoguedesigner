﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]

public class OnLinkControllerDeleted : UnityEvent<Vector2> { }
