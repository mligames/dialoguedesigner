﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class OnPositionChanged : UnityEvent<Vector2> { }
