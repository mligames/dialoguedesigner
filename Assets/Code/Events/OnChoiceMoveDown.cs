﻿using System;
using UnityEngine.Events;

[Serializable]
public class OnChoiceMoveDown : UnityEvent<QuestionChoicePanel> { }

