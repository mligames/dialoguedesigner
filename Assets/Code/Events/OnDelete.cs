﻿using System;
using UnityEngine.Events;

[Serializable]
public class OnDelete : UnityEvent{ }
