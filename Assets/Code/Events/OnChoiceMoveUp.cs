﻿using System;
using UnityEngine.Events;

[Serializable]
public class OnChoiceMoveUp : UnityEvent<QuestionChoicePanel> { }
