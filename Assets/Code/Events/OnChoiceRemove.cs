﻿using System;
using UnityEngine.Events;

[Serializable]
public class OnChoiceRemove : UnityEvent<QuestionChoicePanel> { }
