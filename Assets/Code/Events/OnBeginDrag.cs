﻿using System;
using UnityEngine.Events;

[Serializable]
public class OnBeginDrag : UnityEvent { }